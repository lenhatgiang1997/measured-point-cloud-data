﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;


namespace Generate_pointcloud.Filter.Tests
{
    [TestClass()]
    public class PlaneRecognitionTests
    {
        [TestMethod]
        [TestCase(@"D:\TestAlgorithms\" + "Test2.csv", 4, 0.01, 12, 
            0.0,0.0,0.0,5.0,5.0,0.0, 5.0, 5.0)]
        [TestCase(@"D:\" + "Contour.csv", 4, 0.1, 23, 135.0, 0.0, 14.5, 0.0, 135.4, 99.5, 14.5, 99.5)]
        public void DetectVerticesTest(string path, int numberLines, double threshold, int coordinate,  
            double a1, double b1, double a2, double b2, double a3, double b3, double a4, double b4)
        {
            var helper = new CSVHelper();
            var points = helper.ReadPoints(path);
            var points2D = new List<Point2D>();
            switch (coordinate)
            {
                case 12:
                    points2D = points.Select(s => new Point2D(s.X, s.Y)).ToList();
                    break;
                case 23:
                    points2D = points.Select(s => new Point2D(s.Y, s.Z)).ToList();
                    break;
                case 13:
                    points2D = points.Select(s => new Point2D(s.X, s.Z)).ToList();
                    break;
            }
            

            var view = new View(ViewType.RightView, points);
            var recogniztion = new PlaneRecognition(view);
            var vertices = recogniztion.DetectVertices(points2D, numberLines,threshold);

            Assert.IsTrue(vertices.Any(s => Math.Abs(s.X - a1) < 0.01 || Math.Abs(s.Y - b1) < 0.01));
            Assert.IsTrue(vertices.Any(s => Math.Abs(s.X - a2) < 0.01 || Math.Abs(s.Y - b2) < 0.01));
            Assert.IsTrue(vertices.Any(s => Math.Abs(s.X - a3) < 0.01 || Math.Abs(s.Y - b3) < 0.01));
            Assert.IsTrue(vertices.Any(s => Math.Abs(s.X - a4) < 0.01 || Math.Abs(s.Y - b4) < 0.01));

        }

        [TestMethod]
        [TestCase(@"D:\" + "smoothRightView.csv")]
        public void SideViewSegmentationTest(string path)
        {
            //Create a side view
            var helper = new CSVHelper();
            var points = helper.ReadPoints(path);
            var rightView = new View(ViewType.RightView, points);
            
            var recog = new PlaneRecognition(rightView);
            recog.Execute();
            var detectedPlanes = recog.DetectedPlanes;

            //Verify the results
            Assert.AreEqual(detectedPlanes.Count, 3);

            foreach (var plane in detectedPlanes)
            {
                Assert.IsTrue(plane.Vertexes.Count >= 4);
            }
        }
    }
}