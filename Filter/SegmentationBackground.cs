﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Filter
{
    public class SegmentationBackground
    {
        private List<Point3D> _mPoint3Ds;
        private double _mThresholdDistance;
        private int _mThresholdPoint;

        public SegmentationBackground(List<Point3D> measuredPoint3Ds, double thresholdDistance, int thresholdPoints)
        {
            _mPoint3Ds = measuredPoint3Ds;
            _mThresholdDistance = thresholdDistance;
            _mThresholdPoint = thresholdPoints;
        }

        public List<Point3D> Execute()
        {
            //var foundPlane = new Plane3D();
            var foundPoints = new List<Point3D>();
            var listLayerPoint = new List<Point3D>();

            foreach (var point in _mPoint3Ds.Where(point => listLayerPoint.All(s => (int) s.Z != (int) point.Z)))
            {
                listLayerPoint.Add(point);
            }

            //listLayerPoint = listLayerPoint.OrderByDescending(s => s.Z).ToList();

            var bestSupport = 0;

            //first support plane
            foreach (var layerPoint in listLayerPoint)
            {
                //reset 
                int support = 0;
                var listTemp = new List<Point3D>();

                //make a plane
                var queryPlane = new Plane3D()
                {
                    Position = layerPoint,
                    NormalVector = new Point3D() {X = 0, Y = 0, Z = (int) layerPoint.Z}
                };
                foreach (var point in _mPoint3Ds.Where(point =>
                    Math.Abs(queryPlane.DistPointToPlane(point)) <= _mThresholdDistance))
                {
                    support++;
                    listTemp.Add(point);
                }

                if (support < bestSupport) continue;
                bestSupport = support;
                /*foundPlane = queryPlane;*/
                foundPoints = listTemp;
                /*if (bestSupport >= _mThresholdPoint) break;*/
            }

            //second support plane
            return foundPoints;
        }

        public List<Point3D> FilterObject()
        {
            var pointsOnFirstPlane = Execute();
            //_mPoint3Ds = _mPoint3Ds.Where(s => !pointsOnFirstPlane.Contains(s)).ToList();
            var filteredPoint = new List<Point3D>();
            foreach (var point in _mPoint3Ds)
            {
                if (Math.Abs(point.Z - pointsOnFirstPlane.First().Z) <= 1) continue;
                filteredPoint.Add(point);
            }

            _mPoint3Ds = filteredPoint;
            var pointsOnSecondPlane = Execute();

            return pointsOnFirstPlane.First().Z > pointsOnSecondPlane.First().Z
                ? pointsOnFirstPlane
                : pointsOnSecondPlane;

        }

    }
}
