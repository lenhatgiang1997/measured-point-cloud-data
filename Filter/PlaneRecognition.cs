﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.MachineLearning;
using Accord.MachineLearning.Geometry;
using Accord.Math;
using Accord.Math.Distances;
using Generate_pointcloud.Algorithm;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Filter
{
    public class PlaneRecognition
    {
        private List<Point3D> _mPoint3Ds;
        private List<Plane3D> _detectedPlanes;
        private ViewType _mType;
        
        public PlaneRecognition(View view)
        {
            _mPoint3Ds = view.Points;
            _detectedPlanes = new List<Plane3D>();
            _mType = view.ViewType;
        }

        public void Execute()
        {
            switch (_mType)
            {
                case ViewType.TopView:
                    TopBotViewSegmentation();
                    break;
                case ViewType.BottomView:
                    TopBotViewSegmentation();
                    break;
                case ViewType.RightView:
                    SideViewSegmentation();
                    break;
                case ViewType.LeftView:
                    SideViewSegmentation();
                    break;
            }
        }

        public List<Plane3D> DetectedPlanes => _detectedPlanes;

        protected void TopBotViewSegmentation()
        {
            var estimator = new RansacPlane(0.5, 0.95);
            var arrayPoint = _mPoint3Ds.Select(s => new Point3((float)s.X, (float)s.Y, (float)s.Z)).ToArray();
            var detectedPlane1 = estimator.Estimate(arrayPoint);
            var inliner = estimator.Inliers;
            var inlinerPoints = new List<Point3D>();
            foreach (var index in inliner)
            {
                inlinerPoints.Add(_mPoint3Ds[index]);
            }
            var plane1 = new Plane3D();
            plane1.AssociationPoints = inlinerPoints;
            //set normal vector
            plane1.NormalVector = new Point3D(){X = detectedPlane1.A, Y = detectedPlane1.B, Z = detectedPlane1.C};
            _detectedPlanes.Add(plane1);
        }

        protected void SideViewSegmentation()
        {
            /*
             * Using x as the depth
             * Detect a plane for each group
             */
            //Divide into 3 groups
            var observations = _mPoint3Ds.Select(s => new[] { s.X, s.Y, s.Z }).ToArray();
            var groups = new List<List<Point3D>>();
            var kMean = 3;
            for (int k = 0; k < kMean; k++)
            {
                groups.Add(new List<Point3D>());
            }
            
            while (groups.OrderByDescending(s => s.Count).First().Count <= 3 * _mPoint3Ds.Count / 4)
            {
                /*for (int k = 0; k < kMean; k++)
                {
                    groups.Add(new List<Point3D>());
                }*/

                foreach (var group in groups)
                {
                    group.Clear();
                }
                //Create K-mean algorithm
                var kMeans = new KMeans(kMean)
                {
                    //Scan with x-depth, so focus more on x orientation.
                    Distance = new WeightedSquareEuclidean(new[] { 10, 0.1, 0.1 }),
                };
                var clusters = kMeans.Learn(observations);
                int[] labels = clusters.Decide(observations);


                for (int i = 0; i < labels.Length; i++)
                {
                    for (int j = 0; j < kMean; j++)
                    {
                        if (labels[i] == j)
                        {
                            groups[j].Add(_mPoint3Ds[i]);
                            break;
                        }
                    }
                }
            }
            
            //For debug
            /*var helper = new CSVHelper();
            for (int x = 0; x < kMean; x++)
            {
               helper.WritePoints("D:" + @"\" + "Group" + $"{x}.csv", groups[x]);
            }*/

            foreach (var group in groups)
            {
                var estimator = new RansacPlane(0.5, 0.95);
                var arrayPoint = group.Select(s => new Point3((float)s.X, (float)s.Y, (float)s.Z)).ToArray();
                var detectedPlane1 = estimator.Estimate(arrayPoint);
                var inliner = estimator.Inliers;
                var inlinerPoints = new List<Point3D>();
                foreach (var index in inliner)
                {
                    inlinerPoints.Add(group[index]);
                }
                var plane1 = new Plane3D();
                plane1.Position = inlinerPoints.First();
                plane1.AssociationPoints = inlinerPoints;
                //set normal vector
                plane1.NormalVector = new Point3D() { X = detectedPlane1.A, Y = detectedPlane1.B, Z = detectedPlane1.C };
                /*
                 * From this view, project all points to y-z
                 */
                var helper = new CSVHelper();
                helper.WritePoints(@"D:\" + "Temp.csv", inlinerPoints);
                /*var points = inlinerPoints.Select(s => new Point3D()
                {
                    X = s.Y, Y = s.Z, Z = s.X
                }).ToList();*/
                var points = inlinerPoints.Select(s => new Point2D(s.Y, s.Z)).ToList();
                //Search for contour
                var concaveHull = new ConcaveHull(points, 10);
                concaveHull.Execute();
                var pointsOnContour = concaveHull.GetPointsOnEdges();
                //Debug
                var point3D = pointsOnContour.Select(s => new Point3D() {X = 40, Y = s.X, Z = s.Y}).ToList();
                helper.WritePoints(@"D:\" + "Temp1.csv", point3D);
                //var points2D = pointsOnContour.Select(s => new Point2D(s.X, s.Y)).ToList();
                //Search for the vertex
                var vertices3D = DetectVertices(pointsOnContour, 4, 0.1).Select(s => new Point3D()
                {
                    Y = s.X, Z = s.Y
                }).ToList();
                //Calculate missing x: A*x + B*y + C*z + d = 0
                double d = -plane1.NormalVector.X * plane1.Position.X - plane1.NormalVector.Y * plane1.Position.Y
                                                                      - plane1.NormalVector.Z * plane1.Position.Z;
                foreach (var vertice in vertices3D)
                {
                    vertice.X = (-plane1.NormalVector.Y * vertice.Y - plane1.NormalVector.Z * vertice.Z - d) /
                                plane1.NormalVector.X;
                }

                plane1.Vertexes = vertices3D;
                _detectedPlanes.Add(plane1);
            }

        }

        public List<Point2D> DetectVertices(List<Point2D> contour, int numberLine, double threshold)
        {
            var result = new List<Point2D>();
            var searchingSpace = contour;
            //Loop through contour, random two points, find supporting points
            Dictionary<Line, List<Point2D>> expectedResult = new Dictionary<Line, List<Point2D>>();
            for (int k = 0; k < numberLine; k++)
            {
                int maxSupport = 0;
                var foundLine = new Line();
                var relatedPoints = new List<int>();
                for (int i = 0; i < searchingSpace.Count - 1; i++)
                {
                    int support = 0;
                    var point1 = searchingSpace[i];
                    var point2 = searchingSpace[i + 1];
                    var line = new Line(point1, point2);
                    line.GenerateEquation();
                    var supportPoints = new List<int>();
                    for (int j = 0; j < searchingSpace.Count; j++)
                    {
                        if(j == i || j == i+1) continue;
                        if (!(line.DistanceToPoint(searchingSpace[j]) <= threshold)) continue;
                        support++;
                        supportPoints.Add(j);
                    }

                    if (support > maxSupport)
                    {
                        maxSupport = support;
                        foundLine = line;
                        relatedPoints = supportPoints;
                    }
                }
                expectedResult.Add(foundLine, relatedPoints.Select(s => searchingSpace[s]).ToList());
                var remain = new List<Point2D>();
                /*for (int i = 0; i < searchingSpace.Count; i++)
                {
                    if (relatedPoints.Contains(i)) continue;
                    remain.Add(searchingSpace[i]);
                }*/
                foreach (var point in searchingSpace)
                {
                    if(foundLine.DistanceToPoint(point) < 0.5) continue;
                    remain.Add(point);
                }
                searchingSpace = remain;
            }
            Dictionary<Point2D, List<Line>> vertices = new Dictionary<Point2D, List<Line>>();
            //Find intersections between lines
            for (int i = 0; i < numberLine - 1; i++)
            {
                var line1 = expectedResult.ElementAt(i).Key;
                for (int j = i + 1; j < numberLine; j++)
                {
                    var line2 = expectedResult.ElementAt(j).Key;
                    var intersection = line1.IntersectionWithOtherLine(line2);
                    if (double.IsNaN(intersection.X) || double.IsNaN(intersection.Y)) continue;
                    vertices.Add(intersection, new List<Line>() { line1, line2 });
                }
            }

            result.AddRange(vertices.Select(s => s.Key).ToList());
            return result;
        }
    }
}
