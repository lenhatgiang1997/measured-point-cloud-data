﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Mapping
{
    public class Mapping
    {
        private TheoreticalBeam _mTheorecticalBeam;
        private RealisticBeam _mRealisticBeam;
        private Dictionary<Plane3D, Plane3D> _mapPlanes;
        private Dictionary<Plane3D, Dictionary<Point3D, Point3D>> _mapVerticesPerPlane;

        public Mapping(TheoreticalBeam theorecticalBeam, RealisticBeam realisticBeam)
        {
            _mTheorecticalBeam = theorecticalBeam;
            _mRealisticBeam = realisticBeam;
        }

        //Map function to match vertices between theoretical beam and realistic beam
        public void map()
        {
            //Map planes of each view
            foreach (var realView in _mRealisticBeam.Views)
            {
                var theoreticalPlanes = _mTheorecticalBeam.GetPlanes(realView.ViewType);
                if (realView.ViewType == ViewType.TopView || realView.ViewType == ViewType.BottomView)
                {
                    //only one plane on this view
                    _mapPlanes.Add(realView.Planes.First(), theoreticalPlanes.First());
                }
                else
                {
                    foreach (var realPlane in realView.Planes)
                    {
                        double total_distance = double.MaxValue;
                        var correspondingPlane = new Plane3D();
                        int index = 0;
                        int removeIndex = 0;
                        //map based on total distance of theoretical vertices to the measured plane.
                        foreach (var theoPlane in theoreticalPlanes)
                        {
                            var distance = theoPlane.Vertexes.Select(s => realPlane.DistPointToPlane(s)).Sum();
                            if (distance < total_distance)
                            {
                                total_distance = distance;
                                correspondingPlane = theoPlane;
                                removeIndex = index;
                            }

                            index++;
                        }
                        //key is theoretical plane, value is realistic plane
                        _mapPlanes.Add(correspondingPlane, realPlane);
                        theoreticalPlanes.RemoveAt(removeIndex);
                    }
                }
                
            }
            //Map vertices on each plane
            foreach (var match in _mapPlanes)
            {
                var theoreticalVertices = match.Key.Vertexes;
                var realisticVertices = match.Value.Vertexes;
                _mapVerticesPerPlane.Add(match.Value, new Dictionary<Point3D, Point3D>());

                foreach (var realVertex in realisticVertices)
                {
                    /*double total_distances = double.MaxValue;
                    var correspondingVertex = new Point3D();
                    //Map based on the total distance of theoretical vertices to the query point
                    foreach (var theoVertex in theoreticalVertices)
                    {
                        total_distances += CalculationHelper.EuclideanDistance3D(theoVertex, realVertex);
                    }*/
                    var correspondingVertex = theoreticalVertices.Select(s =>
                            new KeyValuePair<Point3D, double>(s, CalculationHelper.EuclideanDistance3D(s, realVertex)))
                        .OrderBy(a => a.Value).First().Key;
                    _mapVerticesPerPlane[match.Value].Add(correspondingVertex, realVertex);
                    //remove 
                    theoreticalVertices.RemoveAll(s =>
                        s.X == correspondingVertex.X && s.Y == correspondingVertex.Y && s.Z == correspondingVertex.Z);
                }
            }
        }

    }
}
