﻿using CsvHelper.Configuration.Attributes;

namespace Generate_pointcloud.Geometry
{
    public class Point3D
    {

        [Index(0)]
        public double X { set; get; }
        [Index(1)]
        public double Y { set; get; }

        [Index(2)]
        public double Z { set; get; }

        [Index(3)]
        public double Scalar { set; get;}
    }
}
