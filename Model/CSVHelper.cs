﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    public class CSVHelper
    {
        //private List<Point> measured_points;

        public List<Point3D> ReadPoints(string path)
        {
            var measuredPoints = new List<Point3D>();
            try
            {

                var config = new CsvConfiguration(CultureInfo.InvariantCulture);
                config.Delimiter = ";";
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, config))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    var records = csv.GetRecords<Point3D>();
                    measuredPoints = records.ToList();

                }

            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Can not read a file" + ex);
            }
            return measuredPoints;
        }

        public void WritePoints(string path, List<Point3D> data)
        {
            var config = new CsvConfiguration(CultureInfo.InvariantCulture);
            config.Delimiter = ";";
            using (var writer = new StreamWriter(path))
            using (var csv = new CsvWriter(writer, config))
            {
                csv.WriteRecords(data);
            }
        }
    }
}
