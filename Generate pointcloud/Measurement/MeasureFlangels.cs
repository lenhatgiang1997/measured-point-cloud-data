﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Accord.Math;
using Accord.Math.Decompositions;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Measurement
{
    class MeasureFlangels
    {
        /*  ^    ___________________________
         *y |   |                           |
         *  |   |__________      ___________|
         *  |              |    |
         *  |              |    |
         *  |              |    |
         *  |              |    |
         *  |    __________|    |___________
         *  |   |                           |
         *  |   |___________________________|
         *  |_______________________________________________>x
         */
        private double m_Length;
        private double m_Width;
        private double m_outerGap;
        private double m_innerGap;
        private Line m_bottomLine;
        private Line m_topLine;

        public double getLength() => m_Length;
        public double getWidth() => m_Width;
        public double getOuterGap() => m_outerGap;
        public double getInnerGap() => m_innerGap;

        private Accord.Collections.KDTree<int> m_tree;

        private List<Point2D> _measuredPoint;

        private double[][] _arrayPoints;


        public MeasureFlangels(List<Point2D> measuredPoint)
        {
            _measuredPoint = measuredPoint;
            //Make a KDTree
            int length = _measuredPoint.Count;

            _arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                _arrayPoints[i] = new[] { _measuredPoint.ElementAt(i).X, _measuredPoint.ElementAt(i).Y };

            }
            m_tree = Accord.Collections.KDTree.FromData<int>(_arrayPoints);
        }

        public List<Point2D> FindEdges()
        {
            var pointsOnEdges = new List<Point2D>();
            // To create a tree from a set of points
            //var tree = Accord.Collections.KDTree.FromData<int>(arrayPoints);
            //Find the initial points
            var minX =  _measuredPoint.Min(s => s.X);
            var minY = _measuredPoint.Min(s => s.Y);
            //Find the closest to initial points
            var startPoint = m_tree.Nearest(new[] {minX, minY});
            var currentPosition = new Point2D(startPoint.Position[0], startPoint.Position[1]);
            pointsOnEdges.Add(currentPosition);
            double smallestAngel = 0;
            //while until smallest angel >= 50
            while (smallestAngel <= 90)
            {
                smallestAngel = 180;
                //Find KNN and choose the most negative angel

                var kNN = m_tree.Nearest(new []{ currentPosition.X, currentPosition.Y }, 50);
                //find a point smallest angel
                var selectedPoint = new Point2D();
                foreach (var point in kNN)
                {
                    var nextPosition = new Point2D(point.Node.Position[0], point.Node.Position[1]);
                    if (nextPosition.X <= currentPosition.X)
                    {
                        continue;
                    }
                    //calculate angle
                    var angel = AngleDegree(currentPosition, nextPosition);
                    if (angel <= smallestAngel)
                    {
                        smallestAngel = angel;
                        selectedPoint = nextPosition;
                    }
                }
                //add a nest point with smallest angel
                pointsOnEdges.Add(selectedPoint);
                currentPosition = selectedPoint;
            }
            //using linear regression to find the bottom edges
            m_bottomLine = new Line();
            m_bottomLine.LinearRegression(pointsOnEdges);
            return pointsOnEdges;
        }

        public List<Point2D> FindTopEdge()
        {
            var pointsOnTopEdges = new List<Point2D>();
            //Find the initial points
            var minX = _measuredPoint.Min(s => s.X);
            var MaxY = _measuredPoint.Max(s => s.Y);
            //Find the closest to initial points
            var startPoint = m_tree.Nearest(new[] { minX, MaxY });
            var currentPosition = new Point2D(startPoint.Position[0], startPoint.Position[1]);
            pointsOnTopEdges.Add(currentPosition);
            double largestAngel = -45;
            //while until smallest angel >= 50
            while (largestAngel >= - 45)
            {
                largestAngel = -180;
                //Find KNN and choose the most negative angel

                var kNN = m_tree.Nearest(new[] { currentPosition.X, currentPosition.Y }, 50);
                //find a point smallest angel
                var selectedPoint = new Point2D();
                foreach (var point in kNN)
                {
                    var nextPosition = new Point2D(point.Node.Position[0], point.Node.Position[1]);
                    if (nextPosition.X <= currentPosition.X)
                    {
                        continue;
                    }
                    //calculate angle
                    var angel = AngleDegree(currentPosition, nextPosition);
                    if (angel >= largestAngel)
                    {
                        largestAngel = angel;
                        selectedPoint = nextPosition;
                    }
                }
                //add a nest point with smallest angel
                pointsOnTopEdges.Add(selectedPoint);
                currentPosition = selectedPoint;
                //using linear regression to find the bottom edges
            }
            //using linear regression to find the bottom edges
            m_topLine = new Line();
            m_topLine.LinearRegression(pointsOnTopEdges);
            return pointsOnTopEdges;
        }

        /// <summary>
        /// Find a horizontal edges.Start at random point, find all points that are inline. Base on the length of the edges, identify the edges. 
        /// </summary>
        /// <param name="m_measuredPoint"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public List<Point2D> FindHorizontalEdges()
        {
            List<List<Point2D>> listOfEdges = new List<List<Point2D>>();
            foreach (var point in _measuredPoint)
            {
                var edge = new List<Point2D>();
                edge.Add(point);
                var currentPosition = point;
                /*double smallestAngle = 10;
                while (smallestAngle <= 10)
                {
                    smallestAngle = 11;
                    var kNN = m_tree.Nearest(new[] { currentPosition.X, currentPosition.Y }, 20);
                    //find a point smallest angel
                    var selectedPoint = new Point2D();
                    foreach (var neighbor in kNN)
                    {
                        var nextPosition = new Point2D(neighbor.Node.Position[0], neighbor.Node.Position[1]);
                        if ((int)nextPosition.X == (int)currentPosition.X && (int)nextPosition.Y == (int)currentPosition.Y)
                        {
                            continue;
                        }
                        //calculate angle
                        var angel = AngleDegree(currentPosition, nextPosition);
                        if (angel <= smallestAngle)
                        {
                            smallestAngle = angel;
                            selectedPoint = nextPosition;
                        }
                    }
                    edge.Add(selectedPoint);
                    currentPosition = selectedPoint;
                }*/
                var inline = true;
                //find a neighbor 
                var kNN = m_tree.Nearest(new[] { currentPosition.X, currentPosition.Y }, 2);
                var nextPoint = new Point2D();
                foreach (var neighbor in kNN)
                {
                    if ((int) neighbor.Node.Position[0] == (int) point.X && (int)neighbor.Node.Position[1] == (int)point.Y) continue;
                    nextPoint.X = neighbor.Node.Position[0];
                    nextPoint.Y = neighbor.Node.Position[1];
                }
                edge.Add(nextPoint);
                //var selectedPoint = nextPoint;
                while (inline)
                {
                    var find = m_tree.Nearest(new[] { nextPoint.X, nextPoint.Y }, 50);
                    var sort = find.OrderBy(s => s.Distance);
                    foreach (var checkPoint in sort)
                    {
                        var targetPoint = new Point2D(checkPoint.Node.Position[0], checkPoint.Node.Position[1]);
                        if (edge.Any(s => (int) s.X == (int) targetPoint.X && (int) s.Y == (int) targetPoint.Y))
                        {
                            inline = false;
                            continue;
                        }
                        if (!CheckInline(currentPosition, nextPoint, targetPoint))
                        {
                            inline = false;
                        }
                        // found the closet inline point.
                        else
                        {
                            inline = true;
                            edge.Add(targetPoint);
                            currentPosition = nextPoint;
                            nextPoint = targetPoint;
                            break;
                        }
                    }
                }
                if (edge.Count >= 10)
                {
                    listOfEdges.Add(edge);
                }
            }
            //find the longest edges
            //var longestEdges = listOfEdges.OrderByDescending(s => s.Count).First();
            var longestEdges = new List<Point2D>();
            foreach (var list in listOfEdges)
            {
                foreach (var point in list)
                {
                    longestEdges.Add(point);
                }
            }
            return longestEdges;
        }

        public List<List<Point2D>> FindStraightLines()
        {
            List<List<Point2D>> listOfEdges = new List<List<Point2D>>();
            foreach (var point in _measuredPoint)
            {
                var edge = new List<Point2D> {point};
                var currentPosition = point;
                var inline = true;
                //find a neighbor 
                var kNN = m_tree.Nearest(new[] { currentPosition.X, currentPosition.Y }, 2);
                var nextPoint = new Point2D();
                foreach (var neighbor in kNN)
                {
                    if ((int)neighbor.Node.Position[0] == (int)point.X && (int)neighbor.Node.Position[1] == (int)point.Y) continue;
                    nextPoint.X = neighbor.Node.Position[0];
                    nextPoint.Y = neighbor.Node.Position[1];
                }
                edge.Add(nextPoint);
                while (inline)
                {
                    var find = m_tree.Nearest(new[] { nextPoint.X, nextPoint.Y }, 50);
                    var sort = find.OrderBy(s => s.Distance);
                    foreach (var checkPoint in sort)
                    {
                        var targetPoint = new Point2D(checkPoint.Node.Position[0], checkPoint.Node.Position[1]);
                        if (!CheckInline(currentPosition, nextPoint, targetPoint))
                        {
                            inline = false;
                            continue;
                        }
                        if (edge.Any(s => (int)s.X == (int)targetPoint.X && (int)s.Y == (int)targetPoint.Y))
                        {
                            inline = false;
                        }
                        else
                        {
                            inline = true;
                            edge.Add(targetPoint);
                            currentPosition = nextPoint;
                            nextPoint = targetPoint;
                            break;
                        }
                    }
                }

                if (edge.Count >= 10)
                {
                    listOfEdges.Add(edge);
                }
            }
            return listOfEdges;
        }

        public double CalculateHeight()
        {
            double result = 0.0;
            var straightLines = FindStraightLines();
            //Find top and bottom edges
            var firstEdgePoints = straightLines.OrderByDescending(s => s.Count).First();
            var firstLine = new Line();
            firstLine.LinearRegression(firstEdgePoints);
            var filteredList = new List<List<Point2D>>();
            foreach (var list in straightLines)
            {
                bool add = true;
                foreach (var point in list)
                {
                    if (firstEdgePoints.Any(s => s.X == point.X && s.Y == point.Y))
                    {
                        add = false;
                        break;
                    }
                }

                if (add)
                {
                    filteredList.Add(list);
                }
            }
            var secondEdgePoint = filteredList.OrderByDescending(s => s.Count).First();
            var secondLine = new Line();
            secondLine.LinearRegression(secondEdgePoint);
            //calculate average distance from each point of a edge to another edge.
            double sum = 0;
            foreach (var point in firstEdgePoints)
            {
                sum += secondLine.DistanceToPoint(point);
            }

            result = sum / firstEdgePoints.Count;
            return result;
        }



        public List<Point2D> FindSecondEdges()
        {
            var firstEdges = FindHorizontalEdges();
            var newSearchPoints = new List<Point2D>();
            foreach (var point in _measuredPoint)
            {
                if(firstEdges.Any(s => s.X == point.X && s.Y == point.Y)) continue;
                newSearchPoints.Add(point);
            }

            //return newSearchPoints;
            UpdateTree(newSearchPoints);
            var secondEdges = FindHorizontalEdges();
            return secondEdges;

        }

        private void UpdateTree(List<Point2D> newSource)
        {
            var list = newSource.Select(s => new [] {s.X, s.Y}).ToArray();
            m_tree = Accord.Collections.KDTree.FromData<int>(list);
        }

        //Find angle
        public double AngleDegree(Point2D a, Point2D b)
        {
            double deltaX = b.X - a.X;
            double deltaY = b.Y - a.Y;
            double result = Math.Atan2(deltaY, deltaX);
            if (result < 0)
                result += 2d * Math.PI;
            return result * 180d / Math.PI;
        }

        public double AngleWithHorizontalAxis(Point2D a, Point2D b)
        {
            double deltaY = a.Y - b.Y;
            double distance = Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
            double result = Math.Asin(deltaY / distance);
            if (result < 0)
                result += 2d * Math.PI;
            return result * 180d / Math.PI;
        }

        public bool CheckInline(Point2D a, Point2D b, Point2D c)
        {
            var result =  Math.Abs((a.Y - b.Y) * (a.X - c.X) - (a.Y - c.Y) * (a.X - b.X)) <= 0.001;
            return result;
        }
    }
}
