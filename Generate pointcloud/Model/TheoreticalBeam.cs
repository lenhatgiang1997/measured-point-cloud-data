﻿using System;
using System.Collections.Generic;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    //Mapping plane, 
    //contain equations 
    //6 vertical lines segment : y = 0, y = height, y = height - flangeThickness, y = flangeThickness.
    //6 horizontal lines: x = 0, x = (width - webThickness)/2, x = (width + webThickness)/2, x = width
    //4 curve with radius: I1, I2, I3, I4 : (x - x0)^2 + (y - y0)^2 = R^2
    public class TheoreticalBeam : IBeam
    {
        /*  ^
        *        .                |          .
         * `    .        Top view v         .
         *     .___________________________.
       *y |   |                           |
       *  |   |__________      ___________|
       *  |              |    |
       *  |left side view|    |                <-----  Right side View
       *  |      .       |    |   .          .
       *  |     .        |    |  .          .
       *  |    ._________|    |.___________.
       *  |   |                           |
       *  |   |___________________________|
       *  |_______________________________________________>x
         *                  ^
         *                  |
         *                  |
         *             Bottom view
         *
       */

        private double _mHeight; // distance from outface of a flange to a flange
        private double _mLength; // Length of a beam
        private double _mWidth; // Also the width of the flange
        private double _mThinknessFlange;
        private double _mThinknessWeb;
        private double _mHeightWeb;
        private double _radiusCorner;
        private List<View> _views;


        public TheoreticalBeam(double height, double length, 
            double width, double flangeThickness, double webThickness, double webHeight, double radiusCorner)
        {
            _mHeight = height;
            _mLength = length;
            _mWidth = width;
            _mThinknessFlange = flangeThickness;
            _mThinknessWeb = webThickness;
            _mHeightWeb = webHeight;
            _radiusCorner = radiusCorner;
        }

        public View TopView { get; set; }
        public View BottomView { get; set; }
        public View RightView { get; set; }
        public View LeftView { get; set; }

        /*
         * Create 12 planes of ideal beam
         */
        public void Create()
        {
            /*
             * From the top and bottom view, there is one plane for each view
             */
            //Top Plan 
            var planeTop = new Plane3D
            {
                Vertexes = new List<Point3D>()
             {
                 new Point3D(){X = 0, Y = _mHeight, Z = 0},
                 new Point3D(){X = _mWidth, Y = _mHeight, Z = 0},
                 new Point3D(){X = 0, Y = _mHeight, Z = _mLength},
                 new Point3D(){X = _mWidth, Y = _mHeight, Z = _mLength}
             }
            };
            TopView.ViewType = ViewType.TopView;
            TopView.Planes = new List<Plane3D>()
            {
                planeTop
            };
            //Bottom Plane
            var planeBottom = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                {
                    new Point3D() {X = 0, Y = 0, Z = 0},
                    new Point3D() {X = _mWidth, Y = 0, Z = 0},
                    new Point3D() {X = 0, Y = 0, Z = _mLength},
                    new Point3D() {X = _mWidth, Y = 0, Z = _mLength}
                }
            };
            BottomView.ViewType = ViewType.BottomView;
            BottomView.Planes = new List<Plane3D>()
            {
                planeBottom
            };
            /*
             * View from right side, there are five planes
             */
            //Right plane: plane on right of above flange
            var plane_Right_F_A = new Plane3D()
             {
                 Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = _mWidth, Y = _mHeight, Z = 0},
                     new Point3D(){X = _mWidth, Y = _mHeight - _mThinknessWeb, Z = 0},
                     new Point3D(){X = _mWidth, Y = _mHeight, Z = _mLength},
                     new Point3D(){X = _mWidth, Y = _mHeight - _mThinknessWeb, Z = _mLength},
                 }
             };
             var plane_Right_F_B = new Plane3D()
             {
                 Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = _mWidth, Y = 0, Z = 0},
                     new Point3D(){X = _mWidth, Y = _mThinknessWeb, Z = 0},
                     new Point3D(){X = _mWidth, Y = 0, Z = _mLength},
                     new Point3D(){X = _mWidth, Y = _mThinknessWeb, Z = _mLength},
                 }
             };

             var plane_Right_Under_F = new Plane3D()
             {
                 Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                     new Point3D(){X = _mWidth, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = _mWidth, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                 }
             };

             var plane_Right_Above_F = new Plane3D()
             {
                 Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mThinknessFlange, Z = _mLength},
                     new Point3D(){X = _mWidth, Y = _mThinknessFlange, Z = 0},
                     new Point3D(){X = _mWidth, Y = _mThinknessFlange, Z = _mLength},
                 }
             };

             var plane_Right_Web = new Plane3D()
             {
                 Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mThinknessWeb, Z = 0},
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mThinknessWeb, Z = _mLength},
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth + _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                 }
             };

             RightView.ViewType = ViewType.RightView;
             RightView.Planes = new List<Plane3D>()
             {
                 plane_Right_Above_F, plane_Right_Under_F, plane_Right_Web, plane_Right_F_A,plane_Right_F_B
             };

            /*
            * View from left side, there are five planes
            */
            //Right plane: plane on right of above flange
            var plane_Left_F_A = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = 0, Y = _mHeight, Z = 0},
                     new Point3D(){X = 0, Y = _mHeight - _mThinknessWeb, Z = 0},
                     new Point3D(){X = 0, Y = _mHeight, Z = _mLength},
                     new Point3D(){X = 0, Y = _mHeight - _mThinknessWeb, Z = _mLength},
                 }
            };
            var plane_Left_F_B = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = 0, Y = 0, Z = 0},
                     new Point3D(){X = 0, Y = _mThinknessWeb, Z = 0},
                     new Point3D(){X = 0, Y = 0, Z = _mLength},
                     new Point3D(){X = 0, Y = _mThinknessWeb, Z = _mLength},
                 }
            };

            var plane_Left_Under_F = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                     new Point3D(){X = 0, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = 0, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                 }
            };

            var plane_Left_Above_F = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mThinknessFlange, Z = _mLength},
                     new Point3D(){X = 0, Y = _mThinknessFlange, Z = 0},
                     new Point3D(){X = 0, Y = _mThinknessFlange, Z = _mLength},
                 }
            };

            var plane_Left_Web = new Plane3D()
            {
                Vertexes = new List<Point3D>()
                 {
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mThinknessWeb, Z = 0},
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mThinknessWeb, Z = _mLength},
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = 0},
                     new Point3D(){X = (_mWidth - _mThinknessWeb)/2.0, Y = _mHeight - _mThinknessFlange, Z = _mLength},
                 }
            };

            LeftView.ViewType = ViewType.LeftView;
            LeftView.Planes = new List<Plane3D>()
            {
                plane_Left_Above_F, plane_Left_Under_F, plane_Left_Web, plane_Left_F_A,plane_Left_F_B
            };

        }


        List<View> IBeam.Views
        {
            get => _views;
            set => _views = value;
        }

        public List<Plane3D> GetPlanes(ViewType type)
        {
            switch (type)
            {
                case ViewType.TopView:
                    return TopView.Planes;
                case ViewType.BottomView:
                    return BottomView.Planes;
                case ViewType.RightView:
                    return RightView.Planes;
                case ViewType.LeftView:
                    return LeftView.Planes;
                default:
                    throw new ArgumentNullException("Not valid view!");
            }
        }
    }
}
