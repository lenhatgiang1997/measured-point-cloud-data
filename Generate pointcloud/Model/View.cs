﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    public class View
    {
        public ViewType ViewType { get; set; }
        public List<Plane3D> Planes { get; set; }
        public List<Point3D> Points { get; set; }
        public View(ViewType type, List<Point3D> points)
        {
            ViewType = type;
            Points = points;
        }
    }
}
