﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Algorithm
{
    public class ConcaveHull
    {
        private List<Point2D> _mPoint2Ds;
        private List<Point2D> _mPointOnEdges;
        private double _mAlpha;

        public List<Point2D> GetPointsOnEdges() => _mPointOnEdges;

        public ConcaveHull(List<Point2D> listPoint2Ds, double alpha)
        {
            _mPoint2Ds = listPoint2Ds;
            _mAlpha = alpha;
            _mPointOnEdges = new List<Point2D>();
        }


        public void Execute()
        {

            if (_mPoint2Ds == null || _mPoint2Ds.Count < 2)
            {
                throw new ArgumentException("Invalid points");
            }

            //var filteredList = _mPoints;
            int length = _mPoint2Ds.Count;

            double[][] arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                arrayPoints[i] = new double[] { _mPoint2Ds.ElementAt(i).X, _mPoint2Ds.ElementAt(i).Y };

            }

            // To create a tree from a set of points
            var tree = Accord.Collections.KDTree.FromData<int>(arrayPoints);

            /*// Select all pairs in the set of points
            for (int i = 0; i < _mPoint2Ds.Count - 1; i++)
            {
                for (int j = 0; j < _mPoint2Ds.Count; j++)
                {
                    var a = _mPoint2Ds[i];
                    var b = _mPoint2Ds[j];
                    var alpha_2 = _mAlpha * _mAlpha;
                    if (a == b) continue;
                    var dist = CalculationHelper.EuclideanDistance2D(a, b);
                    if (dist > 2 * _mAlpha) { continue; } // circle fits between points ==> p_i, p_j can't be alpha-exposed                    

                    double x1 = a.X, x2 = b.X, y1 = a.Y, y2 = b.Y; // for clarity & brevity

                    var mid = new Point2D((x1 + x2) / 2, (y1 + y2) / 2);

                    // find two circles that contain p_i and p_j; note that center1 == center2 if dist == 2*alpha
                    var center1 = new Point2D(
                        mid.X + (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (y1 - y2) / dist,
                        mid.Y + (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist
                        );

                    var center2 = new Point2D(
                        mid.X - (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (y1 - y2) / dist,
                        mid.Y - (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist
                        );

                    // check if one of the circles is alpha-exposed, i.e. no other point lies in it
                    bool c1_empty = true, c2_empty = true;
                    for (int k = 0; k < _mPoint2Ds.Count && (c1_empty || c2_empty); k++)
                    {
                        if (_mPoint2Ds[k] == a || _mPoint2Ds[k] == b) { continue; }

                        if ((center1.X - _mPoint2Ds[k].X) * (center1.X - _mPoint2Ds[k].X) + (center1.Y - _mPoint2Ds[k].Y) * (center1.Y - _mPoint2Ds[k].Y) < alpha_2)
                        {
                            c1_empty = false;
                        }

                        if ((center2.X - _mPoint2Ds[k].X) * (center2.X - _mPoint2Ds[k].X) + (center2.Y - _mPoint2Ds[k].Y) * (center2.Y - _mPoint2Ds[k].Y) < alpha_2)
                        {
                            c2_empty = false;
                        }
                    }

                    if (c1_empty || c2_empty)
                    {
                        _mPointOnEdges.Add(a);
                        _mPointOnEdges.Add(b);
                    }
                }
            }*/
            foreach (var point in _mPoint2Ds)
            {
                var queryPoint = new[] { point.X, point.Y };
                var kNN = tree.Nearest(queryPoint, 10);
                //var neighborPoint = new Point2D();
                var neighborPoints = new List<Point2D>();
                foreach (var neighbor in kNN.OrderBy(s => s.Distance))
                {
                    if (Math.Abs(neighbor.Node.Position[0] - queryPoint[0]) <= 0.1 &&
                        Math.Abs(neighbor.Node.Position[1] - queryPoint[1]) <= 0.1) continue;
                    var neighborPoint = new Point2D {X = neighbor.Node.Position[0], Y = neighbor.Node.Position[1]};
                    neighborPoints.Add(neighborPoint);
                }
                if (neighborPoints.Count < 2) continue;
                var cluster = new List<Point2D>();
                var kNN2 = tree.Nearest(queryPoint, 20);
                foreach (var neighbor in kNN2)
                {
                    if ((Math.Abs(neighbor.Node.Position[0] - queryPoint[0]) <= 0.02 &&
                         Math.Abs(neighbor.Node.Position[1] - queryPoint[1]) <= 0.02)/* ||
                        (Math.Abs(neighbor.Node.Position[0] - neighborPoint.X) <= 0.02 &&
                         Math.Abs(neighbor.Node.Position[1] - neighborPoint.Y) <= 0.02)*/) continue;
                    cluster.Add(new Point2D()
                    {
                        X = neighbor.Node.Position[0],
                        Y = neighbor.Node.Position[1]
                    });
                }
                foreach (var neighbor in neighborPoints)
                {
                    var a = point;
                    var b = neighbor;
                    var alpha_2 = _mAlpha * _mAlpha;
                    var dist = CalculationHelper.EuclideanDistance2D(a, b);
                    if (dist > 2 * _mAlpha) { continue; } // circle fits between points ==> p_i, p_j can't be alpha-exposed                    

                    double x1 = a.X, x2 = b.X, y1 = a.Y, y2 = b.Y; // for clarity & brevity

                    var mid = new Point2D((x1 + x2) / 2, (y1 + y2) / 2);

                    // find two circles that contain p_i and p_j; note that center1 == center2 if dist == 2*alpha
                    var center1 = new Point2D(
                        mid.X + (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (y1 - y2) / dist,
                        mid.Y + (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist
                        );

                    var center2 = new Point2D(
                        mid.X - (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (y1 - y2) / dist,
                        mid.Y - (double)Math.Sqrt(alpha_2 - (dist / 2) * (dist / 2)) * (x2 - x1) / dist
                        );

                    // check if one of the circles is alpha-exposed, i.e. no other point lies in it
                    bool c1_empty = true, c2_empty = true;
                    for (int k = 0; k < cluster.Count && (c1_empty || c2_empty); k++)
                    {
                        if ((Math.Abs(cluster[k].X - a.X) < 0.01 && Math.Abs(cluster[k].Y - a.Y) < 0.01) || (Math.Abs(cluster[k].X - b.X) < 0.01 && Math.Abs(cluster[k].Y - b.Y) < 0.01)) { continue; }

                        if ((center1.X - cluster[k].X) * (center1.X - cluster[k].X) + (center1.Y - cluster[k].Y) * (center1.Y - cluster[k].Y) < alpha_2)
                        {
                            c1_empty = false;
                        }

                        if ((center2.X - cluster[k].X) * (center2.X - cluster[k].X) + (center2.Y - cluster[k].Y) * (center2.Y - cluster[k].Y) < alpha_2)
                        {
                            c2_empty = false;
                        }
                    }

                    if (c1_empty || c2_empty)
                    {
                        if(_mPointOnEdges.Any(s => Math.Abs(s.X - point.X) < 0.01 && Math.Abs(s.Y - point.Y) < 0.01)) continue;
                        _mPointOnEdges.Add(point);
                        //_mPointOnEdges.Add(neighbor);
                    }
                }
            }
        }

    }
}