﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    class Tube
    {
        private List<Point3D> _mPoints3D;
        private double _mRadiusCorner;
        private double _mHeight;
        private double _mWidth;
        private double _mLength;

        public Tube(double height, double width, double length, double radius)
        {
            _mHeight = height;
            _mWidth = width;
            _mLength = length;
            _mRadiusCorner = radius;
            _mPoints3D = new List<Point3D>();
        }

        public List<Point3D> Points => _mPoints3D;

        public void Create()
        {
            //front 4 lines
            for (double x = 0; x <= _mWidth; x += 0.5)
            {
                for (double y = 0; y <= _mHeight; y += 0.5)
                {
                    for (double z = 0; z <= _mLength; z += 0.5)
                    {
                        if (((y == 0 ||  y == _mHeight) && x >= 0 + _mRadiusCorner && x <= _mWidth - _mRadiusCorner)
                        ||(x == 0 || x == _mWidth) && y >= _mRadiusCorner && y <= _mHeight - _mRadiusCorner)
                        {
                            var temp = new Point3D(){X =  x, Y = y, Z = z};
                            _mPoints3D.Add(temp);
                        }
                    }
                }
            }

            // 4 center points
            var I1 = new Point2D(_mRadiusCorner, _mHeight - _mRadiusCorner);
            var I2 = new Point2D(_mRadiusCorner, _mRadiusCorner);
            var I3 = new Point2D(_mWidth - _mRadiusCorner, _mHeight - _mRadiusCorner);
            var I4 = new Point2D(_mWidth - _mRadiusCorner, _mRadiusCorner);

            //Draw curve 1,3 // half to y
            for (double y = _mHeight - _mRadiusCorner; y <= _mHeight - _mRadiusCorner*(1 -Math.Sqrt(2)/2); y += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var x1 = -Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (y - I1.Y) * (y - I1.Y)) + I1.X;
                    var x2 = +Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (y - I3.Y) * (y - I3.Y)) + I3.X;
                    var temp = new Point3D() { X = x1, Y = y, Z = z };
                    var temp1 = new Point3D() { X = x2, Y = y, Z = z };
                    _mPoints3D.Add(temp);
                    _mPoints3D.Add(temp1);
                }
            }
            // half to x 1,2
            for (double x = _mRadiusCorner*(1 - Math.Sqrt(2)/2); x <= _mRadiusCorner; x += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var y1 = Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (x - I1.X) * (x - I1.X)) + I1.Y;
                    var y2 = -Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (x - I2.X) * (x - I2.X)) + I2.Y;
                    var temp = new Point3D() { X = x, Y = y1, Z = z };
                    var temp1 = new Point3D() { X = x, Y = y2, Z = z };
                    _mPoints3D.Add(temp);
                    _mPoints3D.Add(temp1);
                }
            }

            //Draw curve 2,4 // half to y
            for (double y = _mRadiusCorner* (1 - Math.Sqrt(2)/2); y <= _mRadiusCorner; y += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var x1 = -Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (y - I2.Y) * (y - I2.Y)) + I2.X;
                    var x2 = +Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (y - I4.Y) * (y - I4.Y)) + I4.X;
                    var temp = new Point3D() { X = x1, Y = y, Z = z };
                    var temp1 = new Point3D() { X = x2, Y = y, Z = z };
                    _mPoints3D.Add(temp);
                    _mPoints3D.Add(temp1);
                }
            }

            // half to x 3,4
            for (double x = _mWidth - _mRadiusCorner; x <= _mWidth - _mRadiusCorner*(1 - Math.Sqrt(2)/2); x += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var y1 = Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (x - I3.X) * (x - I3.X)) + I3.Y;
                    var y2 = -Math.Sqrt(_mRadiusCorner * _mRadiusCorner - (x - I4.X) * (x - I4.X)) + I4.Y;
                    var temp = new Point3D() { X = x, Y = y1, Z = z };
                    var temp1 = new Point3D() { X = x, Y = y2, Z = z };
                    _mPoints3D.Add(temp);
                    _mPoints3D.Add(temp1);
                }
            }

        }

    }
}
