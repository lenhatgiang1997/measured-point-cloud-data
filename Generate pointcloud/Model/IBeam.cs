﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    public interface IBeam
    {
        List<View> Views { get; set; }
        List<Plane3D> GetPlanes(ViewType type);

        //Dimensions

    } 
    
}
