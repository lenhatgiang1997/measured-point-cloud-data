﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Filter
{
    public class StatisticalOutlierRemoval
    {
        private Accord.Collections.KDTree<int> _mKdTree;
        private List<Point3D> _measuredPoint;
        private double[][] _arrayPoints;

        private int _kMean;
        private double _mulThres;

        public StatisticalOutlierRemoval(List<Point3D> measuredPoint3Ds, int kMean, double mulThres)
        {
            _kMean = kMean;
            _mulThres = mulThres;
            _measuredPoint = measuredPoint3Ds;
            //Make a KDTree
            int length = _measuredPoint.Count;

            _arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                _arrayPoints[i] = new[] { _measuredPoint.ElementAt(i).X, _measuredPoint.ElementAt(i).Y, _measuredPoint.ElementAt(i).Z};

            }
            _mKdTree = Accord.Collections.KDTree.FromData<int>(_arrayPoints);
        }

        public List<Point3D> Execute()
        {
            var filteredPoints = new List<Point3D>();
            var statisticalPoints = new Dictionary<Point3D, double>();
            //var listDistances = new List<double>();

            foreach (var point in _measuredPoint)
            {
                var queryPoint = new [] { point.X, point.Y, point.Z };
                var kNN = _mKdTree.Nearest(queryPoint, _kMean);
                var cluster = new List<Point3D>();
                foreach (var neighbor in kNN)
                {
                    if (neighbor.Node.Position[0] == queryPoint[0] && neighbor.Node.Position[1] == queryPoint[1]
                                                                   && neighbor.Node.Position[2] == queryPoint[2])
                    {
                        continue;
                    }
                    cluster.Add(new Point3D()
                    {
                        X = (int)neighbor.Node.Position[0],
                        Y = (int)neighbor.Node.Position[1],
                        Z = (int)neighbor.Node.Position[2],
                        Scalar = point.Scalar
                    });
                }
                double sum = 0.0;
                double averageDistance = 0.0;
                
                foreach (var neighborPoint in cluster)
                {
                    sum += CalculationHelper.EuclideanDistance3D(point, neighborPoint);
                }
                //Calculate average distances
                averageDistance = sum / cluster.Count;
                statisticalPoints.Add(point, averageDistance);
            }
            //Calculate mean,std of average distances 
            var listDistances = statisticalPoints.Select(s => s.Value).ToList();
            var mean = listDistances.Average();
            var stdDeviation = CalculationHelper.StdDev(listDistances, true);
            var threshold = mean + _mulThres * stdDeviation;

            foreach (var statisticPoint in statisticalPoints)
            {
                if (statisticPoint.Value <= threshold)
                {
                    filteredPoints.Add(statisticPoint.Key);
                }
            }

            return filteredPoints;
        }
    }
}
