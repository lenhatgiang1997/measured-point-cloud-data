﻿using System;
using System.Collections.Generic;
using System.Linq;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Calculation
{
    public static class CalculationHelper
    {
        public static double StandardDeviation3D(List<Point3D> groupPoints)
        {
            //Get centroid
            var num = groupPoints.Count;
            double meanX = groupPoints.Select(s => s.X).Sum() / num;
            double meanY = groupPoints.Select(s => s.Y).Sum() / num;
            double meanZ = groupPoints.Select(s => s.Z).Sum() / num;
            var meanPoint = new Point3D()
            {
                X = meanX,
                Y = meanY,
                Z = meanZ
            };
            // Distances of each point to the centroid
            var distances = new List<double>();
            foreach (var point in groupPoints)
            {
                distances.Add(Math.Pow(EuclideanDistance3D(point, meanPoint), 2));
            }
            //return standard deviation
            return StdDev(distances, false);
        }

        public static double StdDev(IEnumerable<double> values, bool isSample)
        {
            // Get the mean.
            var enumerable = values as double[] ?? values.ToArray();
            double mean = enumerable.Sum() / enumerable.Count();

            // Get the sum of the squares of the differences
            // between the values and the mean.
            double sumOfSquares = enumerable.Sum(v => (v - mean) * (v - mean));
            if (isSample)
            {
                return Math.Sqrt(sumOfSquares / (enumerable.Count() - 1));
            }
            return Math.Sqrt(sumOfSquares / enumerable.Count());
        }

        public static double DistPointToPlane(Point3D aPoint, double a, double b, double c, double d)
        {
            var num = Math.Abs(a * aPoint.X + b * aPoint.Y + c * aPoint.Z + d);
            var deNum = Math.Sqrt(a * a + b * b + c * c);
            return num / deNum;
        }

        public static  double EuclideanDistance3D(Point3D a, Point3D b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2) + Math.Pow(a.Z - b.Z, 2));
        }
        public static double EuclideanDistance2D(Point2D a, Point2D b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

    }
}
