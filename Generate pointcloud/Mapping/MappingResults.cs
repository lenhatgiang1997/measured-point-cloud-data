﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Mapping
{
    public class MappingResults
    {
        public TheoreticalBeam TheoreticalBeam { get; set; }
        public RealisticBeam RealisticBeam { get; set; }
        public Dictionary<Plane3D, Dictionary<Point3D, Point3D>> VerticesMappingResult { get; set; }
        public Dictionary<Plane3D, Plane3D> PlaneMappingResults { get; set; }
    }
}
