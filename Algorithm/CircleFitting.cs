﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;
using MathNet.Numerics.LinearAlgebra;

namespace Generate_pointcloud.Algorithm
{
    public class CircleFitting
    {
        private List<Point2D> _mSetOfPoints;
        private double _radius;
        private Point2D _centerPoint;

        public double GetRadius => _radius;
        public Point2D GetCenter => _centerPoint;

        public CircleFitting(List<Point2D> setOfPoints)
        {
            _mSetOfPoints = setOfPoints;
        }

        public void calculateRadius()
        {
            var meanX = _mSetOfPoints.Select(s => s.X).Average();
            var meanY = _mSetOfPoints.Select(s => s.Y).Average();

            double uu = 0.0;
            double vv = 0.0;
            double uv = 0.0;
            double uuu = 0.0;
            double uvv = 0.0;
            double vvv = 0.0;
            double vuu = 0.0;

            foreach (var point in _mSetOfPoints)
            {
                var u = point.X - meanX;
                var v = point.Y - meanY;
                uu += Math.Pow(u, 2);
                vv += Math.Pow(v, 2);
                uv += u * v;
                uuu += u * u * u;
                vvv += v * v * v;
                uvv += u * v * v;
                vuu += v * u * u;
            }

            //Solve matrix equation
            var a = Matrix<double>.Build.DenseOfArray(new double[,]
            {
                {uu, uv },
                {uv, vv}
            });

            var b = Vector<double>.Build.Dense(new double[] { 0.5 * (uuu + uvv), 0.5* (vvv + vuu)});

            var x = a.Solve(b);
            _centerPoint = new Point2D(x[0] + meanX, x[1] + meanY);

            _radius = Math.Sqrt(Math.Pow(x[0], 2) + Math.Pow(x[1], 2) + (uu + vv) / _mSetOfPoints.Count);

        }

    }
}
