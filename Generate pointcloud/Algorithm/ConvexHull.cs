﻿using System.Collections.Generic;
using System.Linq;
using Generate_pointcloud.Geometry;
using MIConvexHull;

namespace Generate_pointcloud.Algorithm
{
    public class ConvexHull
    {
        private List<Point2D> _mListOfPoints;
        public ConvexHull(List<Point2D> listOfPoints)
        {
            _mListOfPoints = listOfPoints;
        }

        public static double cross(Point2D O, Point2D A, Point2D B)
        {
            return (A.X - O.X) * (B.Y - O.Y) - (A.Y - O.Y) * (B.X - O.X);
        }

        public List<Point2D> GetConvexHull(List<Point2D> points)
        {
            if (points == null)
                return null;

            if (points.Count <= 1)
                return points;

            int n = points.Count, k = 0;
            List<Point2D> H = new List<Point2D>(new Point2D[2 * n]);

            points.Sort((a, b) =>
                a.X == b.X ? a.Y.CompareTo(b.Y) : a.X.CompareTo(b.X));

            // Build lower hull
            for (int i = 0; i < n; ++i)
            {
                while (k >= 2 && cross(H[k - 2], H[k - 1], points[i]) <= 0.5)
                    k--;
                H[k++] = points[i];
            }

            // Build upper hull
            for (int i = n - 2, t = k + 1; i >= 0; i--)
            {
                while (k >= t && cross(H[k - 2], H[k - 1], points[i]) <= 0.5)
                    k--;
                H[k++] = points[i];
            }

            return H.Take(k - 1).ToList();
        }

    }
}
