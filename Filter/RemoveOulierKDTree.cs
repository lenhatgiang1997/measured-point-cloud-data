﻿using System;
using System.Collections.Generic;
using System.Linq;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Filter
{
    public class RemoveOulierKDTree
    {
        private List<Point3D> _mPoints;
        private int _mKmean;
        public RemoveOulierKDTree(List<Point3D> rawList, int kMean)
        {
            _mPoints = rawList;
            _mKmean = kMean;
        }

        public List<Point3D> Execute()
        {
            //var filteredList = _mPoints;
            int length = _mPoints.Count;

            double[][] arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                arrayPoints[i] = new double[] { _mPoints.ElementAt(i).X, _mPoints.ElementAt(i).Y, _mPoints.ElementAt(i).Z };

            }

            // To create a tree from a set of points
            var tree = Accord.Collections.KDTree.FromData<int>(arrayPoints);

            //List of retained points
            var filteredPoint = new List<Point3D>();

            foreach (var point in _mPoints)
            {
                var queryPoint = new double[] {point.X, point.Y, point.Z};
                var kNN = tree.Nearest(queryPoint, _mKmean);
                var cluster = new List<Point3D>();
                foreach (var neighbor in kNN)
                {
                    cluster.Add(new Point3D()
                    {
                        X = (int)neighbor.Node.Position[0],
                        Y = (int)neighbor.Node.Position[1],
                        Z = (int)neighbor.Node.Position[2],
                        Scalar = point.Scalar
                    });
                }
                /*if (!IsOutlier(cluster, point, _mThreshold))
                {
                    filteredPoint.Add(point);
                }*/
                filteredPoint.Add(Centroid(cluster));
            }
            /*var helper = new CSVHelper();
            helper.WritePoints(@"D:\" + "smoothRightView.csv", filteredPoint);*/
            return filteredPoint;
        }

        public bool IsOutlier(List<Point3D> cluster, Point3D aPoint, double threshold)
        {
            bool result = false;
            cluster.RemoveAt(0);
            //Average distance of a point to its k nearest
            double averageDistance = 0.0;
            foreach (var point in cluster)
            {
                averageDistance += EuclideanDistance(aPoint, point);
            }
            averageDistance = averageDistance / cluster.Count;
            //local density
            double sumExp = 0;
            foreach (var point in cluster)
            {
                sumExp += Math.Exp(-EuclideanDistance(aPoint, point) / averageDistance);
            }
            var localDensity = sumExp / cluster.Count;
            var probability = 1.0 - localDensity;
            //var lambda = 0.6;/*0.0025 * averageDistance*/;

            if (probability > threshold)
            {
                result = true;
            }
            return result;
        }

        public double EuclideanDistance(Point3D a, Point3D b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2) + Math.Pow(a.Z - b.Z, 2));
        }

        public Point3D Centroid(List<Point3D> cluster)
        {
            double sumX = 0.0;
            double sumY = 0.0;
            double sumZ = 0.0;
            int n = cluster.Count;
            foreach (var point in cluster)
            {
                sumX += point.X;
                sumY += point.Y;
                sumZ += point.Z;
            }

            return new Point3D()
            {
                Scalar = 0.0,
                X = sumX / n,
                Y = sumY/n,
                Z = sumZ/n
            };
        }
    }


}
