﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Algorithm
{
    public class RANSAC
    {
        private List<Point3D> m_listOfPoints;
        private double m_probability;
        private double m_threshold;
        private Random m_rands;
        private Accord.Collections.KDTree<int> m_tree;
        private double[][] _arrayPoints;

        public List<Point3D> m_pointsOnSelectedPlane;
        
        public RANSAC(List<Point3D> measuredPoint3Ds, double probability, double threshold)
        {
            //initial process
            m_listOfPoints = measuredPoint3Ds;
            m_probability = probability;
            m_threshold = threshold;
            m_rands = new Random();
            m_pointsOnSelectedPlane = new List<Point3D>();
            //build KD tree
            int length = measuredPoint3Ds.Count;

            _arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                _arrayPoints[i] = new[] { measuredPoint3Ds.ElementAt(i).X, measuredPoint3Ds.ElementAt(i).Y , measuredPoint3Ds.ElementAt(i).Z};

            }
            m_tree = Accord.Collections.KDTree.FromData<int>(_arrayPoints);
        }

        public Plane3D Execute()
        {
            int N = (int) (Math.Log10(1 - 0.9) / Math.Log10(1 - Math.Pow(0.15,3)));
            var m = m_listOfPoints.Count;
            var selectedPlane = new Plane3D(new Point3D(), new Point3D(), new Point3D());
            int bestSupport = 0;
            //select 3 points random
            for (int i = 0; i < N; i++)
            {
                //reset 
                int support = 0;
                var listTemp = new List<Point3D>();
                /*//choose 3 random index
                var index1 = m_rands.Next(0, (m - 1)/3);
                var index2 = m_rands.Next((m -1) / 3, 2*(m - 1)/3);
                var index3 = m_rands.Next(2* (m - 1) /3, m-1);
                // 3 random points
                var point1 = m_listOfPoints.ElementAt(index1);
                var point2 = m_listOfPoints.ElementAt(index2);
                var point3 = m_listOfPoints.ElementAt(index3);*/
                
                //choose a random point, then find 2 other neighbor points
                var randPoint = m_listOfPoints.ElementAt(m_rands.Next(0, m));
                var neighborsPoint = new List<Point3D>();
                //KDTree to find neighbors
                var queryPoint = new[] {randPoint.X, randPoint.Y, randPoint.Z};
                var neighbors = m_tree.Nearest(queryPoint, 3);
                foreach (var neighbor in neighbors)
                {
                    if (neighbor.Node.Position[0] == queryPoint[0] && neighbor.Node.Position[1] == queryPoint[1]
                                                                   && neighbor.Node.Position[2] == queryPoint[2])
                    {
                        continue;
                    }
                    neighborsPoint.Add(new Point3D()
                    {
                        X = (int)neighbor.Node.Position[0],
                        Y = (int)neighbor.Node.Position[1],
                        Z = (int)neighbor.Node.Position[2],
                    });
                }
                if(neighborsPoint.Count < 2) continue;
                //make a plane
                var queryPlane = new Plane3D(randPoint,
                    neighborsPoint.ElementAt(0),
                    neighborsPoint.ElementAt(1));
                foreach (var point in m_listOfPoints)
                {
                    if (Math.Abs(queryPlane.DistPointToPlane(point)) <= 0.5)
                    {
                        support++;
                        listTemp.Add(point);
                    }
                }

                if (support >= bestSupport)
                {
                    bestSupport = support;
                    selectedPlane = queryPlane;
                    m_pointsOnSelectedPlane = listTemp;
                }
            }
            return selectedPlane;
        }
    }
}
