﻿using System;
using System.Collections.Generic;
using System.IO;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    public class Create_plane
    {
        public void create()
        {
            //Front
            var pointsFront = new List<Point3D>();
            for (int x = 0; x <= 50; x++)
            {
                for (int y = 0; y <= 30; y++)
                {
                    if ((x >= 0 && x < 20 || x > 30 && x <= 50) && y > 10 && y < 20)
                    {
                        continue;
                    }
                    var temp = new Point3D
                    {
                        X = x,
                        Y = y,
                        Z = 0,
                        Scalar = 15
                    };

                    pointsFront.Add(temp);
                }
            }

            string frontStr = null;
            foreach (var point in pointsFront)
            {
                frontStr += point.X + ";" + point.Y + ";" + point.Z + ";" + point.Scalar + Environment.NewLine;

            }

            //Back
            var pointsBack = new List<Point3D>();
            for (int x = 0; x <= 50; x++)
            {
                for (int y = 0; y <= 30; y++)
                {
                    if ((x >= 0 && x < 20 || x > 30 && x <= 50) && y > 10 && y < 20)
                    {
                        continue;
                    }
                    var temp = new Point3D
                    {
                        X = x,
                        Y = y,
                        Z = 100,
                        Scalar = 15
                    };

                    pointsBack.Add(temp);
                }
            }

            string backStr = null;
            foreach (var point in pointsBack)
            {
                backStr += point.X + ";" + point.Y + ";" + point.Z + ";" + point.Scalar + Environment.NewLine;

            }

            //Sides
            var pointSides = new List<Point3D>();
            for (int x = 0; x <= 50; x++)
            {
                for (int y = 0; y <= 30; y++)
                {
                    for (int z = 1; z <= 100; z++)
                    {
                        if (y == 30 
                            || y == 0 
                            || y == 20 && (x <= 20 || x >= 30) 
                            || y == 10 && (x <= 20 || x >= 30) 
                            || x == 0 && (y <=10 || y >=20)
                            || x == 20 && y >= 10 && y <=20
                            || x == 30 && y >= 10 && y <= 20
                            || x == 50 && (y <= 10 || y >= 20))
                        {
                            var temp = new Point3D
                            {
                                X = x,
                                Y = y,
                                Z = z,
                                Scalar = 150
                            };

                            pointSides.Add(temp);
                        }
                    }
                }
                
            }

            string sidesStr = null;
            foreach (var point in pointSides)
            {
                sidesStr += point.X + ";" + point.Y + ";" + point.Z + ";" + point.Scalar + Environment.NewLine;

            }

            File.WriteAllText(@"C:\Users\G.Le\Desktop\" + "plane.csv", frontStr + backStr + sidesStr);

        }
    }
}
