﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generate_pointcloud.Model
{
    public enum ViewType
    {
        TopView,
        BottomView,
        RightView,
        LeftView,
    }
}
