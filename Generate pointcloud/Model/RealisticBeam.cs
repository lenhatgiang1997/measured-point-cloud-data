﻿
using System;
using Generate_pointcloud.Geometry;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Generate_pointcloud.Model
{
    public class RealisticBeam : IBeam
    {
        private List<Point3D> _mPoints;
        
        private double _mHeight; // distance from outface of a flange to a flange
        private double _mLength; // Length of a beam
        private double _mWidth; //  Width of the flange
        private double _mThinknessFlange;
        private double _mThinknessWeb;
        private double _mHeightWeb;
        private double _radiusCorner;

        public RealisticBeam(double height, double length, double width, double tf, double tw, double radius)
        {
            _mHeight = height;
            _mLength = length;
            _mWidth = width;
            _mThinknessFlange = tf;
            _mThinknessWeb = tw;
            _radiusCorner = radius;
            _mPoints = new List<Point3D>();
        }

        public RealisticBeam(List<Point3D> pointsList)
        {
            _mPoints = pointsList;
        }

        public RealisticBeam()
        {
            Views = new List<View>();
        }

        public List<Point3D> Points => _mPoints;

        public void Create()
        {
            //Create a front
            //Assume the lower flange start from x = 0;
            var centralPoint = _mWidth / 2;
            var halfTw = _mThinknessWeb / 2;
            for (double x = 0; x <= _mWidth; x += 0.5)
            {
                for (double y = 0; y <= _mHeight; y += 0.5)
                {
                    for (double z = 0; z <= _mLength; z+= 0.5)
                    {
                        if (y == _mHeight
                            || y == 0.0
                            //|| y == _mHeight - _mThinknessFlange && (x <= centralPoint - halfTw - _radiusCorner || x >= centralPoint + halfTw + _radiusCorner)
                            //|| y == _mThinknessFlange && (x <= centralPoint - halfTw - _radiusCorner || x >= centralPoint + halfTw + _radiusCorner)
                            || x == 0 && (y <= _mThinknessFlange || y >= _mHeight - _mThinknessFlange)
                            || x == centralPoint - halfTw && y >= _mThinknessFlange + _radiusCorner && y <= _mHeight - _mThinknessFlange - _radiusCorner
                            || x == centralPoint + halfTw && y >= _mThinknessFlange + _radiusCorner && y <= _mHeight - _mThinknessFlange - _radiusCorner
                            || x == _mWidth && (y <= _mThinknessFlange || y >= _mHeight - _mThinknessFlange))
                        {
                            var temp = new Point3D
                            {
                                X = x,
                                Y = y,
                                Z = z,
                            };

                            _mPoints.Add(temp);
                        }
                    }
                }
            }

            // Draw corner
            var I1 = new Point2D(centralPoint - halfTw - _radiusCorner, _mHeight - _mThinknessFlange - _radiusCorner);
            var I2 = new Point2D(centralPoint - halfTw - _radiusCorner, _mThinknessFlange + _radiusCorner);
            var I3 = new Point2D(centralPoint + halfTw + _radiusCorner, _mHeight - _mThinknessFlange - _radiusCorner);
            var I4 = new Point2D(centralPoint + halfTw + _radiusCorner, _mThinknessFlange + _radiusCorner);

            //Draw curve 1,3
            for (double y = _mHeight - _mThinknessFlange - _radiusCorner; y <= _mHeight - _mThinknessFlange; y += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var x1 = Math.Sqrt(_radiusCorner * _radiusCorner - (y - I1.Y) * (y - I1.Y)) + I1.X;
                    var x2 = -Math.Sqrt(_radiusCorner * _radiusCorner - (y - I3.Y) * (y - I3.Y)) + I3.X;
                    var temp = new Point3D() {X = x1, Y = y, Z = z};
                    var temp1 = new Point3D(){X = x2, Y = y, Z = z};
                    _mPoints.Add(temp);
                    _mPoints.Add(temp1);
                }
            }
            //Draw curve 3,4
            for (double y = _mThinknessFlange ; y <= _mThinknessFlange + _radiusCorner; y += 0.5)
            {
                for (double z = 0; z <= _mLength; z += 0.5)
                {
                    var x1 = Math.Sqrt(_radiusCorner * _radiusCorner - (y - I2.Y) * (y - I2.Y)) + I2.X;
                    var x2 = -Math.Sqrt(_radiusCorner * _radiusCorner - (y - I4.Y) * (y - I4.Y)) + I4.X;
                    var temp = new Point3D() { X = x1, Y = y, Z = z };
                    var temp1 = new Point3D() { X = x2, Y = y, Z = z };
                    _mPoints.Add(temp);
                    _mPoints.Add(temp1);
                }
            }
        }

        public List<View> Views { get; set; }
        public List<Plane3D> GetPlanes(ViewType type)
        {
            switch (type)
            {
                case ViewType.TopView:
                    return Views.SingleOrDefault(s => s.ViewType == ViewType.TopView)?.Planes;
                case ViewType.BottomView:
                    return Views.SingleOrDefault(s => s.ViewType == ViewType.BottomView)?.Planes;
                case ViewType.RightView:
                    return Views.SingleOrDefault(s => s.ViewType == ViewType.RightView)?.Planes;
                case ViewType.LeftView:
                    return Views.SingleOrDefault(s => s.ViewType == ViewType.LeftView)?.Planes;
                default:
                    throw new ArgumentNullException("Not valid view!");
            }
        }
    }
}

