﻿using System;
using System.Collections.Generic;
using Generate_pointcloud.Geometry;
namespace Generate_pointcloud.Measurement
{
    public class MeasurementFunctions
    {
        private List<Point3D> _mContour;
        public MeasurementFunctions(List<Point3D> contour)
        {
            _mContour = contour;
        }

        public double AnglePlane(Plane3D a, Plane3D b, bool returnDegree)
        {
            var vector1 = a.NormalVector;
            var vector2 = b.NormalVector;

            var num = vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
            var u2 = vector1.X * vector1.X + vector1.Y * vector1.Y + vector1.Z * vector1.Z;
            var v2 = vector2.X * vector2.X + vector2.Y * vector2.Y + vector2.Z * vector2.Z;

            var denum = Math.Sqrt(u2 * v2);

            var angle = Math.Acos(num / denum);
            if(returnDegree) angle *= 360.0 / (2 * Math.PI);
            return angle;
        }


    }
}
