﻿using System;
using System.IO;

namespace Generate_pointcloud.Model
{
    class Create_measure_point
    {
        public Create_measure_point()
        {

        }

        public void Create()
        {
            // AB
            for (int i = 0; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 0, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // AM
            for (int i = 0; i <= 10; i++)
            {
                var point = FormPointWithScalar(0, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // BC
            for (int i = 0; i <= 10; i++)
            {
                var point = FormPointWithScalar(50, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // CD
            for (int i = 30; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 10, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // DE
            for (int i = 10; i <= 20; i++)
            {
                var point = FormPointWithScalar(30, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // EF
            for (int i = 30; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 20, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // FG
            for (int i = 20; i <= 30; i++)
            {
                var point = FormPointWithScalar(50, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // GH
            for (int i = 0; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 30, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // HJ 
            for (int i = 20; i <= 30; i++)
            {
                var point = FormPointWithScalar(0, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // JK
            for (int i = 0; i <= 20; i++)
            {
                var point = FormPointWithScalar(i, 20, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // KL
            for (int i = 10; i <= 20; i++)
            {
                var point = FormPointWithScalar(20, i, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // LM
            for (int i = 0; i <= 20; i++)
            {
                var point = FormPointWithScalar(i, 10, 0, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }


            //z direction: (50;0)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(50, 0, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (50;10)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(50, 10, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (50;20)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(50, 20, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (50;30)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(50, 30, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (0;30)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(0, 30, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (0;20)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(0, 20, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }
            //z direction: (0;10)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(0, 10, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }//z direction: (0;0)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(0, 0, i, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }


            /*
             * At z = 100
             */

            // AB
            for (int i = 0; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 0, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // AM
            for (int i = 0; i <= 10; i++)
            {
                var point = FormPointWithScalar(0, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // BC
            for (int i = 0; i <= 10; i++)
            {
                var point = FormPointWithScalar(50, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // CD
            for (int i = 30; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 10, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // DE
            for (int i = 10; i <= 20; i++)
            {
                var point = FormPointWithScalar(30, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // EF
            for (int i = 30; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 20, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // FG
            for (int i = 20; i <= 30; i++)
            {
                var point = FormPointWithScalar(50, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // GH
            for (int i = 0; i <= 50; i++)
            {
                var point = FormPointWithScalar(i, 30, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // HJ 
            for (int i = 20; i <= 30; i++)
            {
                var point = FormPointWithScalar(0, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // JK
            for (int i = 0; i <= 20; i++)
            {
                var point = FormPointWithScalar(i, 20, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // KL
            for (int i = 10; i <= 20; i++)
            {
                var point = FormPointWithScalar(20, i, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            // LM
            for (int i = 0; i <= 20; i++)
            {
                var point = FormPointWithScalar(i, 10, 100, 0) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //inside edges
            //z direction: (30;10)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(30, 10, i, 15) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }

            //z direction: (30;20)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(30, 20, i, 15) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }
            //z direction: (20;10)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(20, 10, i, 15) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }
            //z direction: (20;20)
            for (int i = 0; i <= 100; i++)
            {
                var point = FormPointWithScalar(20, 20, i, 15) + Environment.NewLine;
                File.AppendAllText(@"C:\Users\G.Le\Desktop\" + "test.dat.csv", point);
            }
        }


        static string FormPointWithScalar(int x, int y, int z, int scal)
        {
            return x + ";" + y + ";" + z + ";" + scal;
        }
    }
}
