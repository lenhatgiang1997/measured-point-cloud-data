﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Accord.MachineLearning;
using Accord.MachineLearning.Geometry;
using Accord.Math;
using Accord.Math.Distances;
using Generate_pointcloud.Algorithm;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.ConvexHullMaster;
using Generate_pointcloud.Filter;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Measurement;
using Generate_pointcloud.Model;
using PclSharp;
using PclSharp.Filters;
using PclSharp.IO;
using View = Generate_pointcloud.Model.View;

namespace Generate_pointcloud
{
    class Program
    {
        static void Main(string[] args)
        {
            /*var reader = new ReadMeasuredData();
            var measuredPoints = reader.ReadPoints();
            foreach (var point in measuredPoints)
            {
                Console.WriteLine(@"x: {0}, y: {1}, z: {2}", point.X, point.Y, point.Z);
            }

            Console.ReadLine();*/
            //Draw theory beam
            /*var creater = new Create_plane();
            creater.create();*/

            /*var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"C:\Users\G.Le\Desktop\" + "plane.csv");
            var makeNoise = new Make_noise();
            var pointsWithNoise = makeNoise.Create(measuredPoints);
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "planeWithNoise.csv", pointsWithNoise);*/

            /*var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"C:\Users\G.Le\Desktop\" + "planeWithNoise.csv");
            var reduceNoise = new Remove_Outlier(measuredPoints, 50, 0.5);
            reduceNoise.filter();
            var kDTreeFilter = new RemoveOulierKDTree(measuredPoints, 5, 0.5);
            kDTreeFilter.Execute();*/

            //TestPlane();

            //Measurement();

            //Segmentation();

            //TestFittingCircle();

            //Make a realistic I-beam
            /*var helper = new CSVHelper();
            var IBeam = new IBeam(150.0, 100, 75.0, 10.0, 5.0, 5);
            IBeam.Create();
            helper.WritePoints(@"D:\" + "IBeam4.csv", IBeam.Points);
            var makeNoise = new Make_noise();
            var pointWithNoise = makeNoise.Create(IBeam.Points);
            helper.WritePoints(@"D:\" + "IBeam4Noise.csv", pointWithNoise);*/


            /*var tube = new Tube(75, 50, 20, 5);
            tube.Create();
            var makeNoise = new Make_noise();
            var pointsWithNoise = makeNoise.Create(tube.Points);*/

            /*var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"D:\" + "IBeam4Noise.csv");
            var removal = new StatisticalOutlierRemoval(measuredPoints, 50, 2);
            var filteredPoint = removal.Execute();
            helper.WritePoints(@"D:\" + "FilteredPointsIBeam4.csv", filteredPoint);*/

            //GetRightViewSegmentation();
            //TestAlpha();
            //TestConcaveHull();
            TestContour();

            //CreatePlaneWithHoles();

            /*Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DrawLine());*/


        }

        public static void GetRightView()
        {
            var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"D:\" + "FilteredPointsIBeam4.csv");

            measuredPoints = measuredPoints.Where(s => (s.X > 39 && s.Y > 1 && Math.Abs(s.Y - 150) > 1) ||Math.Abs(s.X - 75) < 1).ToList();

            helper.WritePoints(@"D:\" + "RightViewIBeam4.csv", measuredPoints);
        }

        public static void GetRightViewSegmentation()
        {
            /*var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"D:\" + "RightViewIBeam4.csv");
            var rightView = new View();
            rightView.ViewType = ViewType.RightView;
            rightView.Points = measuredPoints;
            var segmentation = new PlaneRecognition(rightView);
            foreach (var plane in segmentation.DetectedPlanes)
            {
                //Find contour
                var point2Ds = plane.AssociationPoints.Select(s => new Point2D(s.Y, s.Z)).ToList();
                var concaveHull = new Algorithm.ConcaveHull(point2Ds, 10);
                concaveHull.Execute();
                var pointsOnEdges = concaveHull.GetPointsOnEdges().Select(s => new Point3D() { X = 40, Y = s.X, Z = s.Y })
                    .ToList();
                var cSVelper = new CSVHelper();
                cSVelper.WritePoints(@"D:\" + "pointsEdgeConcaveRightView1.csv", pointsOnEdges);
            }*/
        }

        public static void TestSmooth()
        {
            var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"D:\" + "FirstGroup.csv");
            var smooth = new SmoothNoise(10, measuredPoints);
            measuredPoints = smooth.Execute();
            helper.WritePoints(@"D:\" + "Smothies.csv", measuredPoints);
        }

        public static void TestPlane()
        {
            var helper = new CSVHelper();
            var measuredPoints = helper.ReadPoints(@"C:\Users\G.Le\Desktop\" + "planeWithNoise.csv");
            //Create plane: z = 0;
            var plane = new Plane3D()
            {
                Position = new Point3D() { X = 0, Y = 0, Z = 5000},
                NormalVector = new Point3D() { X=0, Y=0, Z=1}
            };
            var projectedPoint = new List<Point3D>();
            foreach (var point in measuredPoints)
            {
                if (Math.Abs(plane.DistPointToPlane(point)) <= 4000)
                {
                    projectedPoint.Add(plane.ProjectPointToPlane(point));
                }
            }
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "projectedPoint.csv", projectedPoint);

        }

        public static void Measurement()
        {
            var helper = new CSVHelper();
            var measuredPoints3D = helper.ReadPoints(@"C:\Users\G.Le\Desktop\" + "projectedPoint.csv");
            var measuredPoints2D = measuredPoints3D.Select(s => new Point2D(s.X, s.Y)).ToList();
            var measureTool = new MeasureFlangels(measuredPoints2D);
            var z = measuredPoints3D.First().Z;
            /*var bottomEdges3D =
                measureTool.FindEdges().Select(s => new Point3D() {Scalar = 0, X = s.X, Y = s.Y, Z = z}).ToList();
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "bottomEdge.csv", bottomEdges3D);*/
            /*var topEdges3D =
                measureTool.FindTopEdge().Select(s => new Point3D() { Scalar = 0, X = s.X, Y = s.Y, Z = z }).ToList();
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "topEdge.csv", topEdges3D);*/
            //measureTool.FindHorizontalEdges();
            /*var longestEdges3D =
                measureTool.CalculateHeight().Select(s => new Point3D() { Scalar = 0, X = s.X, Y = s.Y, Z = z }).ToList();
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "longestEdges.csv", longestEdges3D);*/

            measureTool.CalculateHeight();

            /*List<Point2D> listPoints = new List<Point2D>();
            for (int i = 0; i < 10; i++)
            {
                listPoints.Add(new Point2D(i,i));
            }

            var measure = new MeasureFlangels(listPoints);
            measure.FindHorizontalEdges();*/


        }

        public static void TestAlpha()
        {
            /*var cSVelper = new CSVHelper();
            var measuredPoints3D = cSVelper.ReadPoints(@"D:\" + "IBeamFront.csv");
            var point2Ds = measuredPoints3D.Select(s => new Point2D(s.X, s.Y)).ToList();
            var concaveHull = new ConcaveHull(point2Ds, 1.5);
            concaveHull.Execute();
            var pointsOnEdges = concaveHull.GetPointsOnEdges().Select(s => new Point3D() {X = s.X, Y = s.Y, Z = 0})
                .ToList();
            cSVelper.WritePoints(@"D:\" + "pointsEdgeConcave.csv", pointsOnEdges);*/
        }

        /*public static void TestConcaveHull()
        {
            var cSVelper = new CSVHelper();
            var measuredPoints3D = cSVelper.ReadPoints(@"D:\" + "IBeamFront.csv");
            var noiser = new Make_noise();
            measuredPoints3D = noiser.Create(measuredPoints3D);
            var denoise = new StatisticalOutlierRemoval(measuredPoints3D, 10, 0.5);
            measuredPoints3D = denoise.Execute();
            cSVelper.WritePoints(@"D:\" + "IBeamFrontNoise.csv", measuredPoints3D);
            var point2Ds = measuredPoints3D.Select(s => new Point2D(s.X, s.Y)).ToList();
            var listNodes = new List<Node>();
            for (int i = 0; i < point2Ds.Count; i++)
            {
                listNodes.Add(new Node(point2Ds[i].X, point2Ds[i].Y, i));
            }

            Hull.SetConvexHull(listNodes);
            Hull.SetConcaveHull(0.3, 60);
            var pointsOnEdges = Hull.hull_concave_edges.Select(s =>
                new Point3D() { X = s.nodes[0].x, Y = s.nodes[0].y ,Z = 0 }).ToList();
            pointsOnEdges.AddRange(Hull.hull_concave_edges.Select(s =>
                new Point3D() { X = s.nodes[1].x, Y = s.nodes[1].y, Z = 0 }).ToList());
            cSVelper.WritePoints(@"D:\" + "pointsEdgeConcaveRightView1.csv", pointsOnEdges);
        }*/

        public static void Segmentation()
        {
            var cSVelper = new CSVHelper();
            var measuredPoints3D = cSVelper.ReadPoints(@"D:\" + "img5.csv");
            measuredPoints3D = measuredPoints3D.Where(s => s.X != 0 || s.Y != 0 || s.Z != 0).ToList();
            /*var ransac = new RANSAC(measuredPoints3D, 0.8, 1.0);
            var plane = ransac.Execute();
            var selectedPoints = ransac.m_pointsOnSelectedPlane;*/
            var segment = new SegmentationBackground(measuredPoints3D, 1, 10000);
            var selectedPoints = segment.FilterObject();
            cSVelper.WritePoints(@"D:\" + "filteredPlane.csv", selectedPoints);
        }

        public static void TestContour()
        {
            var cSVelper = new CSVHelper();
            var measuredPoints3D = cSVelper.ReadPoints(@"D:\TestAlgorithms\" + "planeWithHoles.csv");
            /*//Detect plane
            var estimator = new RansacPlane(0.1, 0.95);
            var arrayPoint = measuredPoints3D.Select(s => new Point3((float)s.X, (float)s.Y, (float)s.Z)).ToArray();
            var detectedPlane1 = estimator.Estimate(arrayPoint);
            var inlier = estimator.Inliers;
            var inlierPoints = new List<Point3D>();
            foreach (var index in inlier)
            {
                inlierPoints.Add(measuredPoints3D[index]);
            }
            //smooth noise
            var smooth = new StatisticalOutlierRemoval(inlierPoints, 10, 0.1);
            inlierPoints = smooth.Execute();*/
            //cSVelper.WritePoints(@"D:\" + "InlierPointsTest.csv", inlierPoints);
            //Detect contour
            var point2Ds = measuredPoints3D.Select(s => new Point2D(s.Y, s.Z)).ToList();
            var concave = new ConcaveHull(point2Ds, 10);
            concave.Execute();
            var pointsOnEdges = concave.GetPointsOnEdges().Select(s => new Point3D() { X = 40, Y = s.X, Z = s.Y })
                .ToList();
            cSVelper.WritePoints(@"D:\TestAlgorithms\" + "ContourPlaneWithHoles.csv", pointsOnEdges);

        }

        public static void TestFittingCircle()
        {
            var setOfPoints = new List<Point2D>();
            setOfPoints.Add(new Point2D(0.0, 0.0));
            setOfPoints.Add(new Point2D(0.5, 0.25));
            setOfPoints.Add(new Point2D(1.0, 1.0));
            setOfPoints.Add(new Point2D(1.5, 2.25));
            setOfPoints.Add(new Point2D(2.0, 4.0));
            setOfPoints.Add(new Point2D(2.5, 6.25));
            setOfPoints.Add(new Point2D(3, 9));

            var circleFit = new CircleFitting(setOfPoints);
            circleFit.calculateRadius();
            var centerPoint = circleFit.GetCenter;
        }

        public static void CreatePlaneWithHoles()
        {
            var csvHelper = new CSVHelper();
            var measuredPoints3D = csvHelper.ReadPoints(@"D:\" + "InlierPointsTest.csv");
            var planeWithHoles = new List<Point3D>();
            var center1 = new Point3D(){X = 40, Y = 90, Z = 50};
            var center2 = new Point3D(){X = 40, Y = 80, Z = 99};
            foreach (var point in measuredPoints3D)
            {
                if(CalculationHelper.EuclideanDistance3D(point, center1) <=10 || 
                   CalculationHelper.EuclideanDistance3D(point, center2) <= 10) continue;
                planeWithHoles.Add(point);
            }

            csvHelper.WritePoints(@"D:\TestAlgorithms\" + "planeWithHoles.csv", planeWithHoles);
        }

    }
}
