﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Accord.Statistics.Models.Regression.Linear;

namespace Generate_pointcloud.Geometry
{
    public class Line
    {
        //starting point
        public Point2D A { get; set; }

        //slope
        public double SlopeA { get; set; }
        // y-intercept a
        public double Y_intercept_b { get; set; }
        //End point
        public Point2D B { get; set; }

        public bool IsHorizontal => Math.Abs(A.Y - B.Y) < 0.001f;
        public bool IsVertical => Math.Abs(A.X - B.X) < 0.001f;
        public Line()
        {
            A = new Point2D();
            B = new Point2D();
        }

        public Line(Point2D a, Point2D b)
        {
            A = a;
            B = b;
        }

        public Line(double slope, double yIntercept)
        {
            SlopeA = slope;
            Y_intercept_b = yIntercept;
        }

        public Line(double aX, double aY, double bX, double bY)
        {
            A = new Point2D(aX, aY);
            B = new Point2D(bX, bY);
        }

        public bool IsDiagonal
        {
            get
            {
                if (Math.Abs(A.Y - B.Y) < 0.001f)
                    return false;
                if (Math.Abs(A.X - B.X) < 0.001f)
                    return false;
                return true;
            }
        }

        public double AngleRad
        {
            get
            {
                double deltaX = B.X - A.X;
                double deltaY = B.Y - A.Y;
                double result = Math.Atan2(deltaY, deltaX);
                if (result < 0)
                    result += 2d * Math.PI;
                return result;
            }
        }

        public double Length
        {
            get
            {
                double deltaX = B.X - A.X;
                double deltaY = B.Y - A.Y;
                //Try to prevent Sqrt since expensive
                if (deltaX.IsAboutEqualTo(0) && deltaY.IsAboutEqualTo(0))
                    return 0;
                if (deltaX.IsAboutEqualTo(0))
                    return Math.Abs(deltaY);
                if (deltaY.IsAboutEqualTo(0))
                    return Math.Abs(deltaX);
                //Ok, no other option, calculate it
                return (float)Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
            }
        }

        //A1*x + B1*y = C1
        //A2*x + B2*y = C2
        public Point2D IntersectionWithOtherLine(Line otherLine)
        {
            var a1 = B.Y - A.Y;
            var b1 = A.X - B.X;
            var c1 = a1 * A.X + b1 * A.Y;

            var otherA = otherLine.A;
            var otherB = otherLine.B;

            var a2 = otherB.Y - otherA.Y;
            var b2 = otherA.X - otherB.X;
            var c2 = a2 * otherA.X + b2 * otherA.Y;

            var determine = a1 * b2 - a2 * b1;

            return Math.Abs(determine) < 0.01 // lines are parallel 
                ? new Point2D(Double.NaN, Double.NaN) 
                : new Point2D((b2 * c1 - b1 * c2) / determine, (a1 * c2 - a2 * c1) / determine);
        }

        private static void GenerateEquation(Line line, out double a, out double b)
        {
            a = 0;
            b = 0;
            if (line.IsVertical)
                return;

            double x1 = line.A.X;
            double y1 = line.A.Y;
            double x2 = line.B.X;
            double y2 = line.B.Y;
            a = (y1 - y2) / (x1 - x2); // (x1 - x2)  cannot be 0, that would be a vertical line
            
            if (a.Equals(0))
                b = y1; // could have used y2 also
            else
                b = y1 - a * x1; // could have used x2 and y2 also
        }

        public void GenerateEquation()
        {
            if (IsVertical)
            {
                SlopeA = 1;
                Y_intercept_b = 0;
            }

            else
            {
                double x1 = A.X;
                double y1 = A.Y;
                double x2 = B.X;
                double y2 = B.Y;
                SlopeA = (y1 - y2) / (x1 - x2); // (x1 - x2)  cannot be 0, that would be a vertical line

                if (SlopeA.Equals(0))
                    Y_intercept_b = y1; // could have used y2 also
                else
                    Y_intercept_b = y1 - SlopeA * x1; // could have used x2 and y2 also
            }
           
        }



        public void LinearRegression(List<Point2D> setOfPoints)
        {
            double sumOfX = 0;
            double sumOfY = 0;
            double sumOfXSq = 0;
            double sumOfYSq = 0;
            double sumCodeviates = 0;

            foreach (var point in setOfPoints)
            {
                sumCodeviates += point.X * point.Y;
                sumOfX += point.X;
                sumOfY += point.Y;
                sumOfXSq += point.X * point.X;
                sumOfYSq += point.Y * point.Y;
            }

            var count = setOfPoints.Count;
            var ssX = sumOfXSq - sumOfX * sumOfX / count;
            var sCo = sumCodeviates - sumOfX * sumOfY / count;

            var meanX = sumOfX / count;
            var meanY = sumOfY / count;

            SlopeA = sCo / ssX;
            Y_intercept_b = meanY - SlopeA * meanX;
        }

        public double DistanceToPoint(Point2D point)
        {
            if (IsVertical) return Math.Abs(point.X - A.X);
            if (IsHorizontal) return Math.Abs(point.Y - A.Y);
            var a = SlopeA;
            var b = -1;
            var c = Y_intercept_b;
            return Math.Abs(a * point.X + b * point.Y + c) / Math.Sqrt(a * a + b * b);

        }

    }

    public static class Extension
    {
        public static bool IsAboutEqualTo(this double number, double otherNumber)
        {
            if (double.IsNaN(number) || double.IsNaN(otherNumber))
                throw new ArgumentException("Can not compare NaN");
            if (double.IsPositiveInfinity(number))
                return double.IsPositiveInfinity(otherNumber);
            if (double.IsNegativeInfinity(number))
                return double.IsNegativeInfinity(otherNumber);
            return Math.Abs(number - otherNumber) < GetFloatPrecisionMargin(number);
        }

        private static double GetFloatPrecisionMargin(double number)
        {
            // shift decimal separator 5 places as it is well within the precision of a float according to
            // add minimum margin 0.0001f (precisions for a float of 100f)
            // http://en.wikipedia.org/wiki/Single-precision_floating-point_format
            return Math.Max(0.0001f, Math.Abs(number) / 100000f);
        }
    }
}
