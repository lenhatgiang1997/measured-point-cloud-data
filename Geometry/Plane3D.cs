﻿using System;
using System.Collections.Generic;

namespace Generate_pointcloud.Geometry
{
    public class Plane3D
    {
        public Point3D Position { get; set; }

        public List<Point3D> AssociationPoints { get; set; }

        public List<Point3D> Vertexes { get; set; }

        public Point3D NormalVector { get; set; }

        public int NumberLinesOnContour { get; set; }

        public List<Point3D> PointsOnContour { get; set;}

        public Plane3D(Point3D pointA, Point3D pointB, Point3D pointC)
        {
            //calculate two vector
            var vector1 = new Point3D(){Scalar = 0, X = pointA.X - pointC.X, Y = pointA.Y - pointC.Y, Z = pointA.Z - pointC.Z};
            var vector2 = new Point3D() { Scalar = 0, X = pointB.X - pointC.X, Y = pointB.Y - pointC.Y, Z = pointB.Z - pointC.Z };

            // Calculate a,b,c with cross product
            var abc = new Point3D(){ X = vector1.Y * vector2.Z - vector1.Z * vector2.Y,
                Y = vector1.Z * vector2.X - vector1.X * vector2.Z,
                Z= vector1.X * vector2.Y - vector1.Y * vector2.X };

            //find d in the equation aX + bY + cZ = d
            //var d = abc.X * pointC.X + abc.Y * pointC.Y + abc.Z * pointC.Z;
            // Assign to members
            Position = pointC;
            NormalVector = abc;
        }

        public Plane3D()
        {
            
        }

        public Point3D ProjectPointToPlane(Point3D givenPoint)
        {
            var a = NormalVector.X;
            var b = NormalVector.Y;
            var c = NormalVector.Z;
            var x = givenPoint.X;
            var y = givenPoint.Y;
            var z = givenPoint.Z;
            var t = DistPointToPlane(givenPoint);
            return new Point3D()
            {
                X = x + t*a,
                Y = y + t*b,
                Z = z + t*c
            };
        }

        public double DistPointToPlane(Point3D givenPoint)
        {
            var a = NormalVector.X;
            var b = NormalVector.Y;
            var c = NormalVector.Z;
            var x0 = Position.X;
            var y0 = Position.Y;
            var z0 = Position.Z;
            var x = givenPoint.X;
            var y = givenPoint.Y;
            var z = givenPoint.Z;
            var num = a * (x0 - x) + b * (y0 - y) + c * (z0 - z);
            var deNum = Math.Sqrt(a * a + b * b + c * c);
            return num / deNum;
        }
    }

    
}
