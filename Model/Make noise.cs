﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Model
{
    public class Make_noise
    {
        private readonly Random _Random;

        public Make_noise()
        {
            _Random = new Random();
        }

        public List<Point3D> Create(List<Point3D> measuredPoints)
        {
            int scale = 1;
            //var pointsWithNoise = measuredPoints;
            var noisedPoints = new List<Point3D>();
            foreach (var point in measuredPoints)
            {
                var temp = new Point3D();
                var numXY = GetRandomNumber(0.4, 0.5);
                var numZ = GetRandomNumber(0.5, 1.0);
                //Negative or positive
                var sign = _Random.Next(0,2);
                if (sign == 0)
                {
                    numXY *= -1.0;
                    numZ *= -1.0;
                }
                var choice = _Random.Next(0, 6);
                switch (choice)
                {
                    case 0:
                    {
                        //change in x
                        temp.X = (point.X + numXY)*scale;
                        temp.Y = point.Y * scale;
                        temp.Z = point.Z * scale;
                        break;
                    }
                    case 1:
                    {
                            //change in y
                            temp.X = point.X * scale;
                            temp.Y = (point.Y + numXY) * scale;
                            temp.Z = point.Z * scale;
                            break;
                    }
                    case 2:
                    {
                            //change in z
                            temp.X = point.X * scale;
                            temp.Y = point.Y * scale;
                            temp.Z = (point.Z + numZ) * scale;
                            break;
                    }
                    case 3:
                    {
                            //change in xy
                            temp.X = (point.X + numXY) * scale;
                            temp.Y = (point.Y + numXY) * scale;
                            temp.Z = point.Z * scale;
                            break;
                    }
                    case 4:
                    {
                            //change in xz
                            temp.X = (point.X + numXY) * scale;
                            temp.Y = point.Y * scale;
                            temp.Z = (point.Z + numZ) * scale;
                            break;
                    }
                    case 5:
                    {
                            //change in yz
                            temp.X = point.X * scale;
                            temp.Y = (point.Y + numXY) * scale;
                            temp.Z = (point.Z + numZ) * scale;
                            break;
                    }
                    case 6:
                    {
                            //change in xyz
                            temp.X = (point.X + numXY) * scale;
                            temp.Y = (point.Y + numXY) * scale;
                            temp.Z = (point.Z + numZ) * scale;
                            break;
                    }
                }
                noisedPoints.Add(temp);
            }

            //Add line of outlier
            /*for (double y = 0; y <= 100; y += 0.5)
            {
                noisedPoints.Add(new Point3D(){X = 0, Y = y, Z = - 10});
            }*/

            //Add a outlier plane
            var num = (int) (measuredPoints.Count * 0.1);
            /*for (int i = 0; i <= num; i++)
            {
                var index = _Random.Next(0, num);
                var outlier = new Point3D()
                {
                    X= measuredPoints.ElementAt(index).X + 100,
                    Y = measuredPoints.ElementAt(index).Y + 50,
                    Z = measuredPoints.ElementAt(index).Z

                };
                noisedPoints.Add(outlier);
            }*/

            //Add some outlier points
            for (int i = 0; i <= num * 0.05; i++)
            {
                var temp = new Point3D();
                var randomPoint = measuredPoints.ElementAt(_Random.Next(0, measuredPoints.Count));
                var numXY = GetRandomNumber(0, 50);
                var numZ = GetRandomNumber(0, 50);
                //Negative or positive
                var sign = _Random.Next(0, 2);
                if (sign == 0)
                {
                    numXY *= -1.0;
                    numZ *= -1.0;
                }
                var choice = _Random.Next(0, 6);
                switch (choice)
                {
                    case 0:
                        {
                            //change in x
                            temp.X = (randomPoint.X + numXY) * scale;
                            temp.Y = randomPoint.Y * scale;
                            temp.Z = randomPoint.Z * scale;
                            break;
                        }
                    case 1:
                        {
                            //change in y
                            temp.X = randomPoint.X * scale;
                            temp.Y = (randomPoint.Y + numXY) * scale;
                            temp.Z = randomPoint.Z * scale;
                            break;
                        }
                    case 2:
                        {
                            //change in z
                            temp.X = randomPoint.X * scale;
                            temp.Y = randomPoint.Y * scale;
                            temp.Z = (randomPoint.Z + numZ) * scale;
                            break;
                        }
                    case 3:
                        {
                            //change in xy
                            temp.X = (randomPoint.X + numXY) * scale;
                            temp.Y = (randomPoint.Y + numXY) * scale;
                            temp.Z = randomPoint.Z * scale;
                            break;
                        }
                    case 4:
                        {
                            //change in xz
                            temp.X = (randomPoint.X + numXY) * scale;
                            temp.Y = randomPoint.Y * scale;
                            temp.Z = (randomPoint.Z + numZ) * scale;
                            break;
                        }
                    case 5:
                        {
                            //change in yz
                            temp.X = randomPoint.X * scale;
                            temp.Y = (randomPoint.Y + numXY) * scale;
                            temp.Z = (randomPoint.Z + numZ) * scale;
                            break;
                        }
                    case 6:
                        {
                            //change in xyz
                            temp.X = (randomPoint.X + numXY) * scale;
                            temp.Y = (randomPoint.Y + numXY) * scale;
                            temp.Z = (randomPoint.Z + numZ) * scale;
                            break;
                        }
                }
                noisedPoints.Add(temp);
            }



            //Add noise to the ideal points.
            measuredPoints.AddRange(noisedPoints);


            return measuredPoints;
        }

        public double GetRandomNumber(double minimum, double maximum)
        {
            return _Random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}
