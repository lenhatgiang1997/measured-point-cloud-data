﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Generate_pointcloud.Calculation;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Mapping
{
    public class Query
    {
        private TheoreticalBeam _mTheoreticalBeam;
        private RealisticBeam _mRealisticBeam;
        private MappingResults _mappingResults;

        public Query(MappingResults result)
        {
            _mTheoreticalBeam = result.TheoreticalBeam;
            _mRealisticBeam = result.RealisticBeam;
            _mappingResults = result;
        }

        public Point3D RetrieveMeasuredPoint(Point3D queryPoint, Point3D vertex1, Point3D vertex2,
            Plane3D queryPlane, Point3D vectorDirection, Coordinate type)
        {
            var retrievedPoint = new Point3D();

            var measuredPlane = _mappingResults.PlaneMappingResults[queryPlane];

            var measuredVertex1 = _mappingResults.VerticesMappingResult[measuredPlane][vertex1];
            var measuredVertex2 = _mappingResults.VerticesMappingResult[measuredPlane][vertex2];

            //2 options to find the corresponding point : based on direction or based on the vertices
            //convert everything into 2D
            Point2D intersectionPoint2D;
            double d = -measuredPlane.NormalVector.X * measuredPlane.Position.X - measuredPlane.NormalVector.Y * measuredPlane.Position.Y
                                                                                - measuredPlane.NormalVector.Z * measuredPlane.Position.Z;
            switch (type)
            {
                case Coordinate.XY:
                    intersectionPoint2D = GetIntersectionPoint(new Point2D(queryPoint.X, queryPoint.Y),
                        new Point2D(vectorDirection.X, vectorDirection.Y),
                        new Point2D(measuredVertex1.X, measuredVertex1.Y),
                        new Point2D(measuredVertex2.X, measuredVertex2.Y),
                        measuredPlane.PointsOnContour.Select(s => new Point2D(s.X, s.Y)).ToList());
                    retrievedPoint.X = intersectionPoint2D.X;
                    retrievedPoint.Y = intersectionPoint2D.Y;
                    retrievedPoint.Z = (-measuredPlane.NormalVector.X * retrievedPoint.X - measuredPlane.NormalVector.Y * retrievedPoint.Y - d) /
                                       measuredPlane.NormalVector.Z;
                    break;
                case Coordinate.XZ:
                    intersectionPoint2D = GetIntersectionPoint(new Point2D(queryPoint.X, queryPoint.Z),
                        new Point2D(vectorDirection.X, vectorDirection.Z),
                        new Point2D(measuredVertex1.X, measuredVertex1.Z),
                        new Point2D(measuredVertex2.X, measuredVertex2.Z),
                        measuredPlane.PointsOnContour.Select(s => new Point2D(s.X, s.Z)).ToList());
                    retrievedPoint.X = intersectionPoint2D.X;
                    retrievedPoint.Z = intersectionPoint2D.Y;
                    retrievedPoint.Y = (-measuredPlane.NormalVector.X * retrievedPoint.X - measuredPlane.NormalVector.Z * retrievedPoint.Z - d) /
                                       measuredPlane.NormalVector.Y;
                    break;
                case Coordinate.YZ:
                    intersectionPoint2D = GetIntersectionPoint(new Point2D(queryPoint.Y, queryPoint.Z),
                        new Point2D(vectorDirection.Y, vectorDirection.Z),
                        new Point2D(measuredVertex1.Y, measuredVertex1.Z),
                        new Point2D(measuredVertex2.Y, measuredVertex2.Z),
                        measuredPlane.PointsOnContour.Select(s => new Point2D(s.Y, s.Z)).ToList());
                    retrievedPoint.Y = intersectionPoint2D.Y;
                    retrievedPoint.Z = intersectionPoint2D.Y;
                    retrievedPoint.X = (-measuredPlane.NormalVector.Y * retrievedPoint.Y - measuredPlane.NormalVector.Z * retrievedPoint.Z - d) /
                                       measuredPlane.NormalVector.X;
                    break;

                default:
                    throw new Exception("Not a valid coordinate");
            }

            //Create a line 1 has the direction and corresponding point

            //Find two points closest to the line and lie on two different side of line

            // Form a line 2 of two closest points

            // Intersection point between the line 1 and line 2 is the measuredPoint


            return retrievedPoint;
        }

        public double Deviation(Point3D queryPoint, Point3D vertex1, Point3D vertex2,
            Plane3D queryPlane, Point3D direction, Coordinate type)
        {
            var result = 0.0;
            var correspondingPoint = RetrieveMeasuredPoint(queryPoint, vertex1, vertex2, queryPlane, direction, type);
            result = CalculationHelper.EuclideanDistance3D(correspondingPoint, queryPoint);
            return result;
        }

        public int SignOfLine(Point3D a, Point3D b, Point3D queryPoint, Coordinate type)
        {
            switch (type)
            {
                case Coordinate.XY:
                    return CalculateSignOfLine(new Point2D(a.X, a.Y), new Point2D(b.X, b.Y),
                        new Point2D(queryPoint.X, queryPoint.Y));
                case Coordinate.YZ:
                    return CalculateSignOfLine(new Point2D(a.Y, a.Z), new Point2D(b.Y, b.Z),
                        new Point2D(queryPoint.Y, queryPoint.Z));
                case Coordinate.XZ:
                    return CalculateSignOfLine(new Point2D(a.X, a.Z), new Point2D(b.X, b.Z),
                        new Point2D(queryPoint.X, queryPoint.Z));
                default:
                    return 0;
            }
        }

        private Point2D GetIntersectionPoint(Point2D queryPoint, Point2D vector,
            Point2D measuredVertex1, Point2D measuredVertex2, List<Point2D> contourPoints)
        {
            Point2D intersectionPoint;
            var secondPoint = new Point2D(queryPoint.X + vector.X, queryPoint.Y + vector.Y);
            var queryLine = new Line(queryPoint, secondPoint);
            queryLine.GenerateEquation();

            var relatedPoints = new List<Point2D>();
            var measuredLine = new Line(measuredVertex1, measuredVertex2);
            measuredLine.GenerateEquation();
            //check parrallel
            if (Math.Abs(measuredLine.SlopeA - queryLine.SlopeA) < 0.01)
            {
                var directVector = new Point2D(-vector.Y, vector.X);
                secondPoint = new Point2D(queryPoint.X + directVector.X, queryPoint.Y + directVector.Y);
                queryLine = new Line(queryPoint, secondPoint);
                queryLine.GenerateEquation();
                intersectionPoint = queryLine.IntersectionWithOtherLine(measuredLine);
                return intersectionPoint;
            }
            foreach (var point in contourPoints)
            {
                if(measuredLine.DistanceToPoint(point) <= 0.5) relatedPoints.Add(point);
            }

            relatedPoints = relatedPoints.OrderBy(s => queryLine.DistanceToPoint(s)).ToList();
            /*var topList = new List<Point2D>();
            var botList = new List<Point2D>();*/

            var sidePoint1 = relatedPoints.First(s => CalculateSignOfLine(queryPoint, secondPoint, s) < 0);
            var sidePoint2 = relatedPoints.First(s => CalculateSignOfLine(queryPoint, secondPoint, s) > 0);

            measuredLine = new Line(sidePoint1, sidePoint2);

            intersectionPoint = measuredLine.IntersectionWithOtherLine(queryLine);

            return intersectionPoint;
        }

        private int CalculateSignOfLine(Point2D a, Point2D b, Point2D queryPoint)
        {
            return Math.Sign((queryPoint.X - a.X) * (b.Y - a.Y) - (queryPoint.Y - a.Y) * (b.X - a.X));
        }


    }
}
