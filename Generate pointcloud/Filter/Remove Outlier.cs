﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.MachineLearning;
using Accord.Statistics.Distributions.DensityKernels;
using Generate_pointcloud.Geometry;
using Generate_pointcloud.Model;

namespace Generate_pointcloud.Filter
{
    public class Remove_Outlier
    {
        private List<Point3D> _mPoints;
        private int _mKmean;
        private double _mThreshold;

        public Remove_Outlier(List<Point3D> rawList, int kMean, double threshold)
        {
            _mPoints = rawList;
            _mKmean = kMean;
            _mThreshold = threshold;
        }

        public List<Point3D> filter()
        {
            //var filteredList = _mPoints;
            int length = _mPoints.Count;

            double[][] arrayPoints = new double[length][];

            for (int i = 0; i < length; i++)
            {
                arrayPoints[i] = new double[]{_mPoints.ElementAt(i).X, _mPoints.ElementAt(i).Y, _mPoints.ElementAt(i).Z};
                
            }

            /*KMeans kMeans = new KMeans(_mKmean)
            {
                Distance = new SquareEuclidean(),
                Tolerance = 0.05
            };*/
            MeanShift meanShift = new MeanShift()
            {
                Kernel = new GaussianKernel(3),
                Bandwidth = 0.06,

                // We will compute the mean-shift algorithm until the means
                // change less than 0.05 between two iterations of the algorithm
                Tolerance = 0.05,
                MaxIterations = 10
            };

            //cluster
            //var clusters = kMeans.Learn(arrayPoints);
            var clusters = meanShift.Learn(arrayPoints);
            //calculate mean (centeroid)
            int[] labels = clusters.Decide(arrayPoints);
            // Map a label with a point
            var labelPoints = new Dictionary<int, List<Point3D>>();
            for (int label = 0; label < _mKmean; label++)
            {
                labelPoints.Add(label, new List<Point3D>());
                for (int i = 0; i < length; i++)
                {
                    if (labels[i] == label)
                    {
                        labelPoints[label].Add(_mPoints.ElementAt(i));
                    }
                }
            }
            
            //filter out
            var listAfterFiltered = new List<Point3D>();
            for (int i = 0; i < length; i++)
            {
                /*var cluster =
                    from point in labelPoints
                    where point.Key == labels[i]
                    select point;*/
                var cluster = labelPoints[labels[i]];
                if (!IsOutlier(cluster, _mPoints.ElementAt(i), _mThreshold))
                {
                    listAfterFiltered.Add(_mPoints.ElementAt(i));
                }
            }
            var helper = new CSVHelper();
            helper.WritePoints(@"C:\Users\G.Le\Desktop\" + "filtered6.csv", listAfterFiltered);

            return listAfterFiltered;
        }

        public double LocalDensity(List<Point3D> cluster, Point3D aPoint)
        {
            double result = 0;
            //remove the point out of the cluster
            cluster.Remove(aPoint);
            //Everage distance of a point to its k nearest
            double averageDistance = 0;
            foreach (var point in cluster)
            {
                averageDistance += EuclideanDistance(aPoint, point);
            }
            averageDistance = averageDistance / cluster.Count;
            //local density
            double sumExp = 0;
            foreach (var point in cluster)
            {
                sumExp += Math.Exp(-EuclideanDistance(aPoint, point) / averageDistance);
            }
            result = sumExp / cluster.Count;
            return result;
        }

        public double Probility(List<Point3D> cluster, Point3D aPoint)
        {
            return 1 - LocalDensity(cluster, aPoint);
        }

        public bool IsOutlier(List<Point3D> cluster, Point3D aPoint, double threshold)
        {
            bool result = false;

            //remove the point out of the cluster
            cluster.Remove(aPoint);
            //Average distance of a point to its k nearest
            double averageDistance = 0;
            foreach (var point in cluster)
            {
                averageDistance += EuclideanDistance(aPoint, point);
            }
            averageDistance = averageDistance / cluster.Count;
            //local density
            double sumExp = 0;
            foreach (var point in cluster)
            {
                sumExp += Math.Exp(-EuclideanDistance(aPoint, point) / averageDistance);
            }
            var localDensity = sumExp / cluster.Count;
            var probability = 1 - localDensity;
            //var lambda = 0.6;/*0.0025 * averageDistance*/;

            if (probability > threshold)
            {
                result = true;
            }
            return result;
        }

        public double EuclideanDistance(Point3D a, Point3D b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2) + Math.Pow(a.Z - b.Z, 2));
        }
    }
}
