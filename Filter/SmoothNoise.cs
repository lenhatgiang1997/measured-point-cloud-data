﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.MachineLearning;
using Accord.Math.Distances;
using Generate_pointcloud.Geometry;

namespace Generate_pointcloud.Filter
{
    /*
     * Replace k-mean points by a centroid
     */
    public class SmoothNoise
    {
        private List<Point3D> _mPoints;
        private int _members;
        public SmoothNoise(int members, List<Point3D> points)
        {
            _mPoints = points;
            _members = members;
        }

        public List<Point3D> Execute()
        {
            var observations = _mPoints.Select(s => new[] { s.X, s.Y, s.Z }).ToArray();
            var kMean = _mPoints.Count / _members;
            //Create K-mean algorithm
            var kMeans = new KMeans(kMean)
            {
                //Scan with x-depth, so focus more on x orientation.
                Distance = new WeightedSquareEuclidean(new[] { 10, 0.1, 0.1 })
            };
            var clusters = kMeans.Learn(observations);
            return clusters.Centroids.Select(s => new Point3D() {X = s[0], Y = s[1], Z = s[2]}).ToList();
        }
    }
}
