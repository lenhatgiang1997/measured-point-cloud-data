﻿namespace Generate_pointcloud.Data_Structure
{
    public class KDTree
    {
        public KDTree()
        {
            
        }

        public void Execute()
        {
            double[][] points =
            {
                new double[] { 2, 3, 1 },
                new double[] { 5, 4, 1 },
                new double[] { 9, 6, 2 },
                new double[] { 4, 7, 2 },
                new double[] { 8, 1, 1 },
                new double[] { 7, 2, 2 }
            };

            // To create a tree from a set of points, we use
            var tree = Accord.Collections.KDTree.FromData<int>(points);

            //Given a query point, query for other points which are near this point within a radius

            double[] queryPoint = new double[]{5,3,2};

            var result = tree.Nearest(queryPoint, 3);
            // Node distance
            foreach (var node in result)
            {
                var distance = node.Distance;
                var find = node.Node.Position;
            }
            
        }
    }
}
