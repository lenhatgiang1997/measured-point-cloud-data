﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generate_pointcloud
{
    public partial class DrawLine : Form
    {
        public DrawLine()
        {
            InitializeComponent();
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            var graphic = e.Graphics;
            Pen pen = new Pen(Color.Blue, 5);
            graphic.DrawLine(pen, 100, 100, 200,200);

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
