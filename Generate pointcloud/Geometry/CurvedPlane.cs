﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generate_pointcloud.Geometry
{
    public class CurvedPlane
    {
        public Point3D CenterPosition { get; set; }

        public List<Point3D> AssociationPoints { get; set; }

        public double Radius { get; set; }

        public CurvedPlane(double radius, Point3D centerPoint)
        {
            Radius = radius;
            CenterPosition = centerPoint;
        }
        public CurvedPlane()
        {
            
        }
    }
}
