﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generate_pointcloud.Geometry
{
    public class Point2D
    {
        public double X { get; set; }

        public double Y { get; set; }

        public Point2D(double a, double b)
        {
            X = a;
            Y = b;
        }

        public Point2D()
        {
            
        }
    }
}
